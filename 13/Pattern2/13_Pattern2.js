var myFile = "/13/Pattern2/13_Pattern2_4.csv"
var mapdot_json = "/13/Pattern2/13_Pattern2_4.json" //This pots dots on state map, i can then print the dots im hovering on later in the code

clickedStateId = "FID-State-County"      //TEMPORARY ELEM***WILL USE THIS TO PLUG IN THE LINE IM HIGHLIGHTING. highlightLineOnClick() will set this variable. Will use this variable to deselect the state dot in getClickedLines()*/
var MaxNumberOfCountiesOnGraph = 21//5      //Will determine the size of the set, will se this in brushed. when i click the axis to disable brush, it selects all counties. so i can know when to disable brush by knowing the number of axis//34
var colorColumn = 16

document.getElementById("title").innerHTML = "13 Dangerous Years: Pattern 2";
document.getElementById("myPlot").style.width="1300px";
document.getElementById("myPlot").style.height="700px";



var color_set = d3.scale.linear()
	.range(["red", "green", "blue", "yellow", "black", "orange", "purple"])

var data = []
d3.csv(myFile, function(dataa){
	data = Object.assign(data, dataa);
	defineParallel(data)
});

function defineParallel(data){
    //SETTING LEFT MARGIN SO THAT THE LONGEST NAME ISNT CTU
    var firstCol = data.map(function(d){return d3.values(d)[0]});			//***firstCol=the first column...aka states column
    var textLength = 0;
    firstCol.forEach(function(d){
		if (d.length > textLength)
			textLength = d.length;
    });  
    // GETTING PARALLEL COORDS
    parallelG = d3.parcoords()('#myPlot')
        .data(data)
		.margin({ top: 30, left: 6*textLength, bottom: 40, right: 0 })
		.alpha(3)			   // lightness/density of lines
		.hideAxis(["colors", "Latitude","Longitude"])
		// .mode("queue")
		.rate(5)
	parallel_AddInstructions()
	parallel_Hovering()
	parallel_Render()
}
function parallel_AddInstructions(){
    // ADDING INSTRUCTION TEXT
	var instructions = "Drag on axis to create brush. Click axis to clear brush. Click a label to color data based on the values of that axis. Hover on each line to highlight it."
    d3.select("#myPlot svg").append("text")
        .text(instructions)
        .attr("text-anchor", "middle")
		// .attr("text-decoration", "overline")
        .attr("transform", "translate(" + parallelG.width()/2 + "," + (parallelG.height()) + ")");
	// set the initial colorGraph based on the 2rd column (year 19)
    colorGraph(d3.keys(data[0])[colorColumn]); 	//8 Im going to color via the 2nd column for now: the 1952 column
}            

function parallel_Hovering(){
    // Whenever i move my mouse and i hover one a line, highlight it
    d3.select("#myPlot svg")
        .on("mousemove", function() {
            var mousePosition = d3.mouse(this);		
            highlightLineOnClick(mousePosition, true); //true will also add tooltip
		})
        .on("mouseout", function(){
            cleanTooltip();
            parallelG.unhighlight();
        });
};
function parallel_Render(){
	parallelG.render()			   // draw the line son the graph
	.brushMode("1D-axes")  // enable brushing mode to filter out lines
	.interactive();
}




