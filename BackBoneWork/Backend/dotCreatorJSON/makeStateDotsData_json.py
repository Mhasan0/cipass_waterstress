myFile = "StateParallel_13Normalized_Pattern1.txt"
# Turn pattern1 csv to pattern1.json im adding  so i can plot the dots of the counties in the map in state.js

import numpy as np # will use this to make an array of floats. this array is the value of each state key. The aray hold the index averages for all years
import csv
f = open(myFile) #1) make csv file in a tab delimited .txt
data = []
rowCount = 0
for row in f:
    data_row = row.rstrip().split('\t')  
    data.append(data_row)
    rowCount+=1

stateColumn = 0 #the index of the column the state is is
dataStartColumn = 1 #index where the yearly datas start from 
dataEndColumn = len(data[0]) #end of data column 
columnsZero = dataEndColumn-dataStartColumn+1 #dataEndColumn - dataStartColumn is the pure year data column number + 1 because will add county number to index 0 



topics = {"id",  "agprod", "lat", "lon","color" }
print("[")
for row in range(1, rowCount):
    print("    {")
    for col in range(0,dataEndColumn):
        c = '"' #sinc eim finding it hard to add a " into quaotes"
        if col == 0:
            print("""       "id": """+c+data[row][col]+c+",")
        if col == 15:
            print("""       "lat": """+data[row][col]+",")
        if col == 16:
            print("""       "lon": """+data[row][col]+",")
            print("""       "agprod": """ + "1," )
            print("""       "color": """ + c+"red"+c )
        
    print("    },")
print("]")
