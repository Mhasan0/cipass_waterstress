#using this for testing my small ex. ********** has chnages

#input data: Fid, Column, State, Latitude, Longitude, years
myData = '13_Original.txt' #original dataset but with only the 13 most drough years

# FInd if an element is in any part of an array (2D or 3D or 1D): print(any('a' in sublist for sublist in pattern)) #if 'a' is in the array "pattern", will return true




# myData = 'myExData.txt' #original dataset but with only the 13 most drough years

import numpy as np # will use this to make an array of floats. this array is the value of each state key. The aray hold the index averages for all years
import csv
f = open(myData) #1) make csv file in a tab delimited .txt
data = []
rowCount = 0
for row in f:
    data_row = row.rstrip().split('\t')  
    data.append(data_row)
    rowCount+=1

# MERGING TO GET fid-state-county COLUMNS:
for row in range(0,rowCount): 
    data[row][0] = data[row][0]+"-"+data[row][2]+"-"+data[row][1]
fidMerged = 0   # After merging, the index of fid is 0 when i split the first column using split()
stateMerged = 1
countyMerged = 2
stateColumn = 0 #the index of the column the state is is

# SETING UP SOME VARIABLES FOR USE
dataStartColumn = 5 #index where the yearly datas start from 
dataEndColumn = len(data[0]) #end of data column  (NOT THE INDEX, loop never reaches this number! so index of last col is dataEndColumn-1)
dataColumnsLength = dataEndColumn - 5 #-5 because fid,state,county,long,latid ###############**********chnaged this from 4 to 5 see if works


# #test
# dataStartColumn = 1 #index where the yearly datas start from 
# dataEndColumn = len(data[0]) #end of data column (NOT THE INDEX, loop never reaches this number!)
# dataColumnsLength = dataEndColumn - 1 #the graph column

# FUNCTION TO PRINT 2D ARRAY To Put In csv
def printData(s):
    maximumValue = 0.0
    d = ""
    n= ""
    rowC = len(s) 
    colL = len(s[0])
    for row in range(0, rowC):
        for col in range(0,colL): 
            if (row == 0) and (col>=dataStartColumn and col<dataEndColumn):
                d+= "y"+str(s[row][col])
            else:
                d+= str(s[row][col])
                if col>=dataStartColumn and col<dataEndColumn:
                    if float(s[row][col]) > maximumValue:
                        maximumValue = float(s[row][col])
                        n = s[row][0]
            if (col < colL-1):             
                d+=","
        d+="\n"
    print(d)
    # print("BASE,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0")
    # MAX,0,0,0.016,0.016,0.016,0.016,0.016,0.016,0.016,0.016,0.016,0.016,0.016,0.016,0.016,0

    maximumValue+=0.1*maximumValue
    maxi = ""
    mini = ""
    for col in range(0,colL): 
        if col == colL-2:
            maxi+="0"
            mini+="0"
            break
        if col == 0:
            maxi+="MAX,"
            mini+="MIN,"
        if col>=dataStartColumn-1 and col<dataEndColumn:           
            maxi+=str(maximumValue)+","
            mini+="0,"
        else:
            maxi+="0,"
            mini+="0,"
        

    print(mini)
    print(maxi)

################################ REDUCE COUNTIES WITH ALL ZEROES ALSO GET RID OF COUNTY, STATE,LATITUDE, LONGITUDE ###############################################################
printTheseRows = []
printTheseRows.append(0)
# Figuring out which rows to print. Will eliminate all rows with zeroes for all years
for row in range(1,rowCount): # forget title row
    zeroCount = 0
    for col in range(dataStartColumn, dataEndColumn):
        if float(data[row][col]) < 1.0: #if i were to filter out only zeroes, ther would be 3111 counties left...so im filtering for any county less than 1 ndi over all years
            # if float(data[row][col]) > 0.0:
            #     print(data[row][0])
            zeroCount+=1 
        data[row][col] = round(float(data[row][col]), 7)
    if zeroCount < dataColumnsLength: # if there are 13 data columns and if i get less than 13, say 12 zero counts. then at least one county isnt a 0 and so i can delete it. 
        printTheseRows.append(row)
# PUT THE REDUCED STRING INTO DATA:
s = []
rowCount2 = 0
for row in printTheseRows: 
    rowi = []
    for col in range(0,dataEndColumn):
        if col>stateColumn and col <dataStartColumn-2:  # Getting rid of County and State columns since i combined them already            #####**********changed it to not use numebrs 1 or 2
            continue
        if row == 0:
            rowi.append(str(data[0][col]))
            continue
        rowi.append(str(data[row][col]))
    s.append(rowi)
    rowCount2+=1
data = s
rowCount = rowCount2
dataStartColumn = 3 #**************************add to 13 script  ###################**********chnaged to 1 from 3
dataEndColumn = len(s[0]) 
print(data[0])
print(len(data[0]))
print(len(data))
###################################### NORMALIZE DATA #########################################################************cant normalize data before findign patterns. the top pattern becomes 1.0 and cant do. so moved this block below patterns
# For normilizing: making a max/min array to keep track of all max/min vla of each year
maxy = []
miny = []
for col in range(0, dataStartColumn): 
    maxy.append(0.0) # These are unimportant first spaces. lines up this index and dataStartCol in the for loop below
    miny.append(0.0)

# FIND MIN/MAX OF EACH COLUMNS. WILL BE USED LATER TO NARMALIZE DATA:
for col in range(dataStartColumn, dataEndColumn): 
    maxy.append(-1.0) #initial val to check for this column
    miny.append(10000000.0)
    for row in range(1,rowCount): 
        if float(data[row][col]) >= maxy[col]:
            maxy[col] = float(data[row][col])
        if float(data[row][col]) < miny[col]:
            miny[col] = float(data[row][col])
# USING MAX/MIN DATA TO NORMALIZE DATA
for col in range(dataStartColumn, dataEndColumn): 
    for row in range(1,rowCount):        
        data[row][col] = round((float(data[row][col]) - miny[col]) / (maxy[col] - miny[col]), 10)
dataColumnsLength = len(data[0])

# ###################################### Some functions to use for Pattern FIltering #########################################################
rate = (dataColumnsLength - 1) -2 #doing -2 because im skipping the first and last columns sinc esometimes graphs arnt the samw there # out of the max 12 need to match
similarity = 2 # mean that i want at least 2 other similar graphs (exclusing initial county) to be put into patterns array *********
# FILTERING
def LowestCommonSubStr(str1, str2): 
	LCSuff = [[0 for k in range(len(str2)+1)] for l in range(len(str1)+1)] 
	result = 0
	for i in range(len(str1) + 1): 
		for j in range(len(str2) + 1): 
			if (i == 0 or j == 0): 
				LCSuff[i][j] = 0
			elif (str1[i-1] == str2[j-1]): 
				LCSuff[i][j] = LCSuff[i-1][j-1] + 1
				result = max(result, LCSuff[i][j]) 
			else: 
				LCSuff[i][j] = 0
	return result 

def dictToArray(dictt):
    d = []
    for i in dictt:
        row = []
        row.append(i)
        row.append(dictt[i])
        d.append(row)
    return d

############################## STAGE 1: Behavioral String Arrays: Turn the behavior elements dictionary to behavior string arrays ###############################
# Making the behavior pattern and the percent change strings. If a graph goes "up,down,up" -> behavior strign: "udu"
upDownEle  = dict()    #   dictionary matrix of ups and downs elements
upDownStr  = dict()    #   combined all u and d into a string
changeEle = dict()     #   perchange change between each axis
changeStr = dict()     #   string made from element of perchange change between each axis
for row in range(1,rowCount):
    if (data[row][0] not in upDownEle):
        upDownEle[data[row][0]] = ""
        upDownStr[data[row][0]] = ""
        changeEle[data[row][0]] = ""
        changeStr[data[row][0]] = ""
    upDownTemp = []
    changeTemp = []
    for col in range(dataStartColumn+1, dataEndColumn-1):
        
        #If in the initial column, record its ndi and move on to next col
        if col == dataStartColumn+1: 
            continue
        # Find the percent change up or down. If the denominator will be 0, than make the change 100000
        if data[row][col-1] == 0:
            changeT = 100000
        else:
            changeT = float(round(((float(data[row][col]) - float(data[row][col-1])) / float(data[row][col-1])) * 100,1)) #nvm: ********This is the percent chnage. I made it an int instead of a float because any change that is less than +-1% is very miniscule. I also did this because I get far less hits with a float  
        # Depending on if it went up or down, put in "u" or "d" in the upDown array and the percent change into the changeArray
        if changeT >= 1.0:
            upDownTemp.append("u")
        # elif changeT <1.0 and changeT >-1.0: # les than 1 percent change is very small so i made it count the same
        #     upDownTemp.append("x")
        else:
            upDownTemp.append("d")
        changeTemp.append(str(data[0][col])+"_"+str(changeT))
    # Add the arrays to the Matrix
    upDownEle[data[row][0]] = upDownTemp
    changeEle[data[row][0]] = changeTemp
    # Will now turn the "u" and "d" matrix of arrays into strings
    upDownStrT = ""
    changeStrT = ""
    for i in range(0, len(upDownTemp)):
        upDownStrT += upDownTemp[i]
    for i in range(0, len(changeTemp)):
        changeStrT += changeTemp[i]
        if (i<len(changeTemp)-1):
            changeStrT += "," #will use this to do the chnages
    upDownStr[data[row][0]] = upDownStrT
    changeStr[data[row][0]] = changeStrT
upDown = dictToArray(upDownStr)
change = dictToArray(changeStr)
# print(upDown)
# print(change) #['countyname', 'BehaviorString~BehaviorChangePercentage"]

############################## STAGE 2: Comparison Matrix: making a new matrix where the rows are an array containing the initial county and the least common substr of all other counties related to the initial county ###############################
# Making a new matrix/set of least common behavioral substring
sets = [] # 92x92 matrix:  [['1stCounty,ItsUpDownStr', 'nthCounties, LCSubStr(1st,othersCounties)'], ['2ndCounty,ItsUpDownStr', 'nthCounties, LCSubStr(2nd,othersCounties'] ]
for row in range(0, len(upDown)):
    temp = []
    temp.append(upDown[row][0]) # input the initial county to the array. Will go through all counties in the next for loop and compare to this county
    for i in range(0, len(upDown)):
        if (upDown[i][0] == upDown[row][0]): # if the same county, then skip
            continue
        temp.append(upDown[i][0]+"~"+str(LowestCommonSubStr(upDown[row][1], upDown[i][1])))
    sets.append(temp)   

############################## STAGE 3: Find patterns within that new set (see if comparison counties has a lest common substr of >= "rated". Then add it to a temporary array along with the initial county) ###############################
patterns = []
noPatterns = []
# Go thought the rows of the sets and find all counties that has the "rate" maximum least common substr and add it to "patterns"
for row in range(0, len(sets[0])):  
    # If the initial county is in the 2D patterns array, then skip it since we already covered it:
    if (any(sets[row][0] in sublist for sublist in patterns)): 
        continue
    patternTemp = []
    for col in range(1,len(sets[row])):   # starting from the 1st comarizon thats why im skipping index 0, which is the initial county    
        split = sets[row][col].split('~') # index0 is the county name, index1 is the least common substr
        if (any(split[0] in sublist for sublist in patterns)): # skip the comparison county if its in the pattern array already
            continue
        if (int(split[1]) >= rate): # If the comparison county has a maximum common substring 9compared to initial county) of >= "rate", ass it to the pattern
            patternTemp.append(split[0])
    # If there are at least 2 graphs (including the initial county), then add it to "patterns". Here im just adding the initial county to the temporary array and pushing it to patterns
    if len(patternTemp) >= similarity:                  # This county has at least 1 other county that matches with it, so add it to the patterns array! adding the initial county
        split = sets[row][0].split('~')
        patternTemp.insert(0, split[0])
        patterns.append(patternTemp)
    else:
        split = sets[row][0].split('~')
        noPatterns.append(split[0])
# print("\nPatterns:")
# for row in range(0, len(patterns)):      
#     print(patterns[row])
#     print("\n")







####################################### Filtering Data by Pattern #########################################################
#csv4
#1 = pattern1
#0 = patern2
patternPick = 2
extremes = [
    "1801",  # Virginia-Franklin City"
    "1665",  # Virginia-Bedford City",
    "1675",  # Virginia-Hopewell",
    "1539",  # Virginia-Clifton Forge",
    "1309",  # Virginia-Manassas City",
    "1284",  # Virginia-Falls Chruch",
    "710",   # Massachusetts-Nantucket",
    "690"    # Massachusetts-Dukes"
]
p = ""
s = []
rowCount = len(data)
rowCount2 = 0 
for row in range(0, rowCount):
    rowi = []
    split = data[row][0].split('-')    
    p+=split[1]+","+split[2]+","+split[0]+"\n"
    fid = split[fidMerged]    

    # if ((fid in extremes)): #or (fid in pattern1_EXCLUDE) or (fid in pattern2) or (fid in pattern3)): 
    #     continue
    if row != 0: # Without this, will skip the title
        if data[row][0] not in patterns[patternPick]:
            continue
    else:
        rowCount2 +=1
    
    for col in range(0,dataEndColumn):
        rowi.append(str(data[row][col]))
    s.append(rowi)
data = s
rowCount = len(s) #rowCount2
dataEndColumn = len(data[0]) 
###################################### FILTERING BASED ON PERCENTAGE ###########################################
# print(patterns)
# print(len(patterns))
# print()
# print(change)
# print()
# print(patterns[patternPick][0]) #an element in pattern [0 for row]
# print(change[0][0])#[row][0 for id and 1 for percent]

# print()

# print(any("690-Massachusetts-Dukes" in sublist for sublist in patterns[patternPick]))
# print(not any("690-Massachusetts-Dukes" in sublist for sublist in patterns[patternPick]))

for row in range(0,len(change)):
    if not any(change[row][0] in sublist for sublist in patterns[patternPick]): # If the county is NOT in the pattern set i picked, then skip it
        continue
    if change[row][0] == "398-Idaho-Canyon" or change[row][0] =='1152-California-Colusa' or change[row][0] == '1592-California-Merced':
        print(change[row])

        #Idea do the subtract each col by avgerage and divide by standard div to find which numbers are near each other
###################################### COLOR VIA STATE #########################################################
# Giving each state a value for printing so that i can make a colors column 
stateColors = dict()
stateCount = 0
for row in range(1,rowCount):
    split = data[row][0].split('-')
    if (split[stateMerged] not in stateColors):
        stateCount+=1
        stateColors[split[stateMerged]] = stateCount

for row in range(0,rowCount):
    if row == 0:
        data[0].append("colors")   
        continue 
    split = data[row][0].split('-')
    data[row].append(stateColors[split[stateMerged]])
dataColumnsLength = len(data[0])

#################################### State Dot Map Json ###############################################################

def printJSON(data):
    print("\n\n\n########## JSON ##########\n\n")
    latCol = 1
    lonCol = 2

    topics = {"id",  "agprod", "lat", "lon","color" }
    print("[")
    for row in range(1, rowCount):
        print("    {")
        for col in range(0,dataEndColumn):
            c = '"' #since im finding it hard to add a " into quaotes"
            if col == 0:
                print("""       "id": """+c+str(data[row][col])+c+",")
            if col == latCol:
                print("""       "lat": """+str(data[row][col])+",")
            if col == lonCol:

                print("""       "lon": """+str(data[row][col])+",")
                print("""       "agprod": """ + "1," )
                print("""       "color": """ + c + "red" + c )
        print("    },")
    print("]")

# printData(data)
# printJSON(data)

