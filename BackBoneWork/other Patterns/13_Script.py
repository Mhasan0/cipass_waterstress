# printing directly to 13 and All folders
#input data: Fid, Column, State, Latitude, Longitude, years
myData = '13_0_Original.txt' #original dataset but with only the 13 most drough years

import numpy as np # will use this to make an array of floats. this array is the value of each state key. The aray hold the index averages for all years
import csv
f = open(myData) #1) make csv file in a tab delimited .txt
data = []
rowCount = 0
for row in f:
    data_row = row.rstrip().split('\t')  
    data.append(data_row)
    rowCount+=1

# MERGING TO GET fid-state-county COLUMNS:
for row in range(0,rowCount): 
    data[row][0] = data[row][0]+"-"+data[row][2]+"-"+data[row][1]
fidMerged = 0   # After merging, the index of fid is 0 when i split the first column using split()
stateMerged = 1
countyMerged = 2


# SETING UP SOME VARIABLES FOR USE
stateColumn = 0 #the index of the column the state is is
dataStartColumn = 3 #index where the yearly datas start from 
dataEndColumn = len(data[0]) #end of data column 
dataColumnsLength = dataEndColumn - 5 #-5 because fid,state,column,long,latid

# FUNCTION TO PRINT 2D ARRAY To Put In csv
def printData(s):
    maximumValue = 0.0
    d = ""
    rowC = len(s) 
    colL = len(s[0])
    for row in range(0, rowC):
        for col in range(0,colL): 
            if (row == 0) and (col>=dataStartColumn and col<dataEndColumn):
                d+= "y"+str(s[row][col])
            else:
                d+= str(s[row][col])
                if col>=dataStartColumn and col<dataEndColumn:
                    if float(s[row][col]) > maximumValue:
                        maximumValue = float(s[row][col])
            if (col < colL-1):             
                d+=","
        d+="\n"
    print(d)
    # print("BASE,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0")
    # MAX,0,0,0.016,0.016,0.016,0.016,0.016,0.016,0.016,0.016,0.016,0.016,0.016,0.016,0.016,0

    maximumValue+=0.07*maximumValue
    maxi = ""
    mini = ""
    for col in range(0,colL): 
        if col == colL-2:
            maxi+="0"
            mini+="0"
            break
        if col == 0:
            maxi+="MAX,"
            mini+="MIN,"
        if col>=dataStartColumn-1 and col<dataEndColumn:           
            maxi+=str(maximumValue)+","
            mini+="0,"
        else:
            maxi+="0,"
            mini+="0,"
    print(mini)
    print(maxi)

################################ REDUCE COUNTIES WITH ALL ZEROES ###############################################################
printTheseRows = []
printTheseRows.append(0)
# Figuring out which rows to print
for row in range(1,rowCount): #forget title row
    count = 0
    for col in range(dataStartColumn, dataEndColumn):
        if float(data[row][col]) < 1.0:
            count+=1
        data[row][col] = round(float(data[row][col]), 7)
    if count < dataColumnsLength: #if there are 13 data columns and if i get less than 13, say 12 zero counts. then at least one county isnt a 0 and so i can delete it. 
        printTheseRows.append(row)

# PUT THE REDUCED STRING INTO DATA:
s = []
rowCount2 = 0
for row in printTheseRows: 
    rowi = []
    for col in range(0,dataEndColumn):
        if col==1 or col ==2:
            continue
        if row == 0:
            rowi.append(str(data[0][col]))
            continue
        rowi.append(str(data[row][col]))
    s.append(rowi)
    rowCount2+=1
data = s
rowCount = rowCount2
dataEndColumn = len(s[0]) 

###################################### NORMALIZE DATA #########################################################
# For normilizing: making a max/min array to keep track of all max/min vla of each year
maxy = []
miny = []
for col in range(0, dataStartColumn): 
    maxy.append(0.0) # These are unimportant first spaces. lines up this index and dataStartCol in the for loop below
    miny.append(0.0)
# FIND MIN/MAX OF EACH COLUMNS. WILL BE USED LATER TO NARMALIZE DATA:
for col in range(dataStartColumn, dataEndColumn): 
    maxy.append(-1.0) #initial val to check for this column
    miny.append(10000000.0)
    for row in range(1,rowCount): 
        # if data[row][0] in extremes: #**DONT NORMILIZE WITHOUT MAX VALUES
        #     continue
        if float(data[row][col]) >= maxy[col]:
            maxy[col] = float(data[row][col])
        if float(data[row][col]) < miny[col]:
            miny[col] = float(data[row][col])
# USING MAX/MIN DATA TO NORMALIZE DATA
for col in range(dataStartColumn, dataEndColumn): 
    for row in range(1,rowCount):
        data[row][col] = round((float(data[row][col]) - miny[col]) / (maxy[col] - miny[col]), 10)

dataColumnsLength = len(data[0])

# ###################################### PATTERN PRINT FILTERING #########################################################
# FILTERING
def LowestCommonSubStr(str1, str2): 
	LCSuff = [[0 for k in range(len(str2)+1)] for l in range(len(str1)+1)] 
	result = 0
	for i in range(len(str1) + 1): 
		for j in range(len(str2) + 1): 
			if (i == 0 or j == 0): 
				LCSuff[i][j] = 0
			elif (str1[i-1] == str2[j-1]): 
				LCSuff[i][j] = LCSuff[i-1][j-1] + 1
				result = max(result, LCSuff[i][j]) 
			else: 
				LCSuff[i][j] = 0
	return result 

def dictToArray(dictt):
    d = []
    for i in dictt:
        row = []
        row.append(i)
        row.append(dictt[i])
        d.append(row)
    return d

extremes = [
    "1801",  # Virginia-Franklin City"
    "1309",  # Virginia-Manassas City",
    "1284",  # Virginia-Falls Chruch",
    "1665",  # Virginia-Bedford City",
    "1539",  # Virginia-Clifton Forge",
    "1675",  # Virginia-Hopewell",
    "710",   # Massachusetts-Nantucket",
    "690"    # Massachusetts-Dukes"
]

# DO PATTERNS 1 AND 2 AND 3
pattern1 = [ "1263",
    "1707",
    "833",
    "969",
    "2380",
    "891",
    "841",
    "641",
    "1384",
    "2119",
    "2895",
    "1547",
    "2339",
    "2239",
    "2372",
    "1096",
    "1546",
    "2039",
    "854",
    "2972",
    "866",
    "815",
    "1548",
    "981",
    "1835",
    "2151", 
    "3035"
]
pattern2=[ #made from pattern1
    "1466",
    "3084,"
    "890",
    "3033",
    "1174",
    "3081",
    "2065",
    "3071"     
]
pattern3=[
    # "2396",
    "1868",    
    "1592",
    "1483",
    "1420",    
    "1152"
    # "515",
    # "478"
]
pattern4a = [    #others
    # "2829",
    # "638",
    # "3084",
    # "1939",
    # "914",
    # "890",
    
    # "3033",
    # "904",
    # "2556",

    # "3071",

    # "844",
    "1174",
    "1466",
    "2065",
    "3081",
]

pattern4b = [ #flat top slight up
    "2829",    
    "2889", 
    "2876",   
    # "1096",
    # "1256",
    # "914",
    # "1939",
    # "2556",p
]
pattern4c = [  #spiky
    "621",
    "720"
]
##NOT DONE
pattern6 = [
    "2050",
    "653",
    "1659",
    "1656",
    "956",
    "2925",
    #from 4
    "840",
    "3087",
    "3070",
    "2396",
    "3070",
    "1256",
    "3008",
    "638",
    "904",
    "2829",
    "844",
    "2829"
]



patternChoice = pattern3





upDown  = dict()    #   dictionary matrix of ups and downs elements
upDownStr  = dict() #   combined all u and d into a string
change = dict()     #   perchange change between each axis
for row in range(1,rowCount):
    if (data[row][0] not in upDown):
        upDown[data[row][0]] = ""
        upDownStr[data[row][0]] = ""
        change[data[row][0]] = ""
    upDownA = []
    changeA = []
    for col in range(dataStartColumn, dataEndColumn):
        if col == dataStartColumn: #if in the initial column, record its ndi and move on to next col
            continue
        # if data[row][col-1] == 0:
        #     changeT = 10000
        # else:
        changeT = (data[row][col] - data[row][col-1]) #/ data[row][col-1] *100
        
        # if changeT <= 2 and changeT >= -2:
        #     upDownA.append("x")
        if changeT >= 0:
            upDownA.append("u")
        else:
            upDownA.append("d")
        changeA.append(data[row][col] - data[row][col-1]) #/ data[row][col-1] * 100

    upDown[data[row][0]] = upDownA
    upDownStrT = ""
    for i in range(0, len(upDownA)):
        upDownStrT += upDownA[i]
    upDownStr[data[row][0]] = upDownStrT
    change[data[row][0]] = changeA

upDown = dictToArray(upDownStr)
for row in range(0, len(upDown)):
    e = ""
    for col in range(0, len(upDown[0])):
        e += upDown[row][col]
        e+="     "



sets = [] #92x92 matrix
# [  
#   ['1stCounty,ItsUpDownStr', 'OtherCounties, LCSubStr(1st,others)'],
#   ['2ndCounty,ItsUpDownStr', 'OtherCounties, LCSubStr(2nd,Others'],
# ]
rate = 10# out of 12 need to match
for row in range(0, len(upDown)):
    temp = []
    temp.append(upDown[row][0])#+","+upDown[row][1])
    for i in range(0, len(upDown)):
        if (upDown[i][0] == upDown[row][0]): # if the same county, then skip
            continue
        temp.append(upDown[i][0]+"~"+str(LowestCommonSubStr(upDown[row][1], upDown[i][1])))
    sets.append(temp) 
    # print("SET: "+str(sets[row])+"\n")
# print(len(sets)) #rows
# print(len(sets[0])) #col

patterns = []
noPatterns = []
for row in range(0, len(sets)):  
    split = sets[row][0].split("-")
    fid = split[0]
    if (fid in extremes): # row!=0:  mena that to run this loop if im not in title row. if im in title row it will compare fid (which will be the string "FId" to the fid numbers in the arrays, resulting in error)
        continue

    isBreak = False
    for i in range(0, len(patterns)):# if this county is in the patterns 2d array, skip it since its already in apattern
        if (sets[row][0] in patterns[i]): 
            isBreak = True
            break
    if isBreak == True:
        isBreak = False
        continue   

    if (sets[row][0] in noPatterns):
        continue 
   
    patternT = []
    for col in range(1,len(sets[row])):
        for i in range(0, len(patterns)):# if this county is in the patterns 2d array, skip it since its already in apattern
            if (sets[row][col] in patterns[i]): 
                isBreak = True
                break
            if isBreak == True:
                isBreak = False
                continue  

        if (sets[row][col] in noPatterns):
            continue 

        split = sets[row][col].split('~')
        if (int(split[1]) >= rate):
            # count+=1
            patternT.append(split[0])
    if len(patternT) >= 2:                  #this county has al least 2 other counties that matches with it, so add it to the list
        split = sets[row][0].split('~')
        patternT.insert(0, split[0])
        patterns.append(patternT)
    else:
        split = sets[row][0].split('~')
        noPatterns.append(split[0])



# for row in range(0, len(patterns)):      
#     print(patterns[row])
#     print("\n")
# print(len(patterns))
# print(noPatterns)

    

     






####################################### Pattern Based Re-organizing Data #########################################################

p = ""
s = []
rowCount = len(data) 
for row in range(0, rowCount):
    rowi = []
    
    split = data[row][0].split('-')    
    p+=split[1]+","+split[2]+","+split[0]+"\n"
    fid = split[fidMerged]    

    # ADDING THE TITLE COLUMN:
    if row == 0:# row!=0:  mena that to run this loop if im not in title row. if im in title row it will compare fid (which will be the string "FId" to the fid numbers in the arrays, resulting in error)
        for col in range(0,dataEndColumn):
            rowi.append(data[0][col])
        s.append(rowi)
        continue
    
    # PATTERN FILTER:
    if (fid in extremes) or (fid not in patternChoice): #THIS IS WITH MY MANUAL PRINTING. i notice that i get almost same results as my algorithm
        continue

    # if row != 0: #without this, will skip the title
    #     if data[row][0] not in patterns[1]:
    #         continue

    
    # PRINTING THE PATTERN COUNTIES
    for col in range(0,dataEndColumn): 
        rowi.append(str(data[row][col]))
    s.append(rowi)
data = s
rowCount = len(s) #rowCount2
dataEndColumn = len(data[0]) 
###################################### COLOR VIA STATE #########################################################
# Giving each state a value for printing so that i can make a colors column 
stateColors = dict()
stateCount = 0
for row in range(1,rowCount):
    split = data[row][0].split('-')
    if (split[stateMerged] not in stateColors):
        stateCount+=1
        stateColors[split[stateMerged]] = stateCount

# print(stateColors)
for row in range(0,rowCount):
    if row == 0:
        data[0].append("colors")   
        continue 
    split = data[row][0].split('-')
    data[row].append(stateColors[split[stateMerged]])
dataColumnsength = len(data[0])
# print(data)
#################################### State Dot Map Json ###############################################################

def printJSON(data):
    print("\n\n\n########## JSON ##########\n\n")
    latCol = 1
    lonCol = 2

    topics = {"id",  "agprod", "lat", "lon","color" }
    print("[")
    for row in range(1, rowCount):
        print("    {")
        for col in range(0,dataEndColumn):
            c = '"' #since im finding it hard to add a " into quaotes"
            if col == 0:
                print("""       "id": """+c+str(data[row][col])+c+",")
            if col == latCol:
                print("""       "lat": """+str(data[row][col])+",")
            if col == lonCol:
                print("""       "lon": """+str(data[row][col])+",")
                print("""       "agprod": """ + "1," )
                print("""       "color": """ + c + "red" + c )
        print("    },")
    print("]")


dataStartColumn = 3

printData(data)
# printJSON(data)




    
