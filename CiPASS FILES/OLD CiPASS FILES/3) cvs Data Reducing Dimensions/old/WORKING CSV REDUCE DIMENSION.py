import numpy as np # will use this to make an array of floats. this array is the value of each state key. The aray hold the index averages for all years

data =  [
    ['Alabama', '0.2', '2'], 
    ['Alabama', '0.2', '2'], 
    ['Alabama', '0.3', '2'], 
    ['Alabama', '0.4', '2'], 
    ['A', '0.4', '3']
]# [alambama: 1.1/4, 8.0/4]

rowsZero = 1    #***chage! to 50 states
columnsZero = 2 #***change! number of years + 1 since for loop starts with year 1 

index = dict() # this is an dictionary that has key-value pairs to automatically group all average years for the state
count = 0      # counting number of counties in the state so i can use to to find average
summy = 0.0

for year in range(1,3):     # looping through year column
    print("                            For year "+str(year))
    for row in range(0,5):  # looping through counties rows
        state = data[row][0] #***change 0 to whatever column number that states are in the excell sheet
        # 1) If the state isnt in the dictionary, add it. Also assign it to a value of an array that is the size of the excel sheet columns. populate the aray with zeroes for better calculations
        if state not in index:
            print(state + " not in dictionary so adding it")
            index[state] = np.zeros((rowsZero,columnsZero)) #***MAKING MATRIX OF ZEROES floats!

        # 2) summing up all the columns of the index matrix(summing up water index for the year of the state) with the data of the state's counties for that year <-- basically summing up counties every row i go down at the same time!
        # BREAKDOWN: index[state] [0][year-1]. index[state]: return the value of the state key (which happens to be a matrix of 1D array). [0]: picking first row since a state's dictionary data is only one array. [year-1]: is the column/element in array. since the year loop starts with 1 i subtracted with '-1' to get to the 0th index
        sumy = index[state].sum(axis=0)[year-1] + float(data[row][year]) #axis=0 means to sum up columns of dictionary matrix.
        index[state] [0][year-1] = sumy #adding the refreshed summy to the enty. 
       
        count+=1
        #print(state+":   "+str(index[state][0]))
    summy = 0.0
    count = 0

state = "Alabama"
#print Stuff:
print("\nALABAMA: ")
for i in range(0,len(index['Alabama'])):
    print(index['Alabama'][i])
print("A: ")
for i in range(0,len(index['A'])):
    print(index['A'][i])


#################################append float arrays###########################################
# import numpy as np

# #TEMP1 = [22.4, 14.4, 12.3]
# temperatures = []
# floatTemperatures = np.array(temperatures, dtype = np.float32)
 
# floatTemperatures = np.append(floatTemperatures, 3.0)
# floatTemperatures = np.append(floatTemperatures, 4.0)
# print(floatTemperatures[0])





# #works
# index = dict() # this is an dictionary that has key-value pairs to automatically group all average years for the state
# stateCount = dict()
# sumYr = 0.0    
# count = 0      # counting number of counties in the state so i can use to to find average

# import numpy as np # will use t his to make an array of floats. this array is the value of each state key. The aray hold the index averages for all years
# sumYrTemp = []     # apprantly i need to make this empty array thta i dont even use
# sumYrArray = np.array(sumYrTemp, dtype = np.float32)  # this will be the float array i will append stuff to


# for year in range(1,3):     # looping through year column
#     for row in range(0,4):  # looping through counties rows
#         sumYr = round(sumYr + float(data[row][year]), 8) #***change to 8 or 9. adding the indexes of the current year
#     sumYrArray = np.append(sumYrArray, sumYr) # adding the sum of the index into the float array
#     index[data[row][0]] = sumYrArray
#     sumYr=0
# #print alabama stuff
# for i in range(0,len(index['Alabama'])):
#     print(index['Alabama'][i])



##works
# index = dict()
# sumYr = 0.0
# for year in range(1,3): 
#     for row in range(0,4):
#         sumYr = (round(sumYr + float(data[row][year]), 8))
#     index[data[row][0]] = sumYr
#     print(index[data[row][0]])
#     sumYr=0





# for country in Countries:
#   # For each country from the list check to see whether it is in the dictionary Capitals
#     if country in Capitals:
#         print('The capital of ' + country + ' is ' + Capitals[country])
#     else:
#         print('The capital of ' + country + ' is unknown')