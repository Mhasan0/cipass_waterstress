#Notes: 
# one state is missing (Alaska). 
# i took out the long and latitude columns since it made no sense for states
# Final: Alaska was not included in data set. i had to exclude 'District of Columbia' and 'Virginia' because of the high NDI that had, which messed up data

import numpy as np # will use this to make an array of floats. this array is the value of each state key. The aray hold the index averages for all years
import csv
f = open('FIXED_DATA_NDI.txt') #1) make csv file in a tab delimited .txt
# 0) Put in data matrix:
data = []
rowCount = 0
for row in f:
    data_row = row.rstrip().split('\t')  
    data.append(data_row)
    rowCount+=1

stateColumn = 3 #the index of the column the state is is
dataStartColumn = 6 #index where the yearly datas start from 
dataEndColumn = len(data[0]) #end of data column 
columnsZero = dataEndColumn-dataStartColumn+1 #dataEndColumn - dataStartColumn is the pure year data column number + 1 because will add county number to index 0 
index = dict() # this is an dictionary that has key-value pairs to automatically group all average years for the state    

for year in range(dataStartColumn,dataEndColumn): # looping through year column
    for row in range(1,rowCount):                 # looping through counties rows
        state = data[row][stateColumn] 
        # 1) If the state isnt in the dictionary, add it. Also assign it to a value of an array that is the size of the excel sheet columns. populate the aray with zeroes for better calculations
        if state not in index:
            index[state] = np.zeros((1,columnsZero)) #***MAKING ARRAY OF ZEROES for each state to store the yearly index of each state
        # 2) Counting how many counties in each state came up so we can get the average index values
        if year == dataStartColumn: 
            index[state] [0][0] = index[state] [0][0] + 1 
        # 3) SUMMING THE INDEX FOR EACH STATE. Summing up all the columns of the index matrix(summing up water index for the year of the state) with the data of the state's counties for that year <-- basically summing up counties every row i go down at the same time!
        #BREAKDOWN: index[state] [0][year]. index[state]: return the value of the state key (which happens to be a matrix of 1D array). [0]: picking first row since a state's dictionary data is only one array. [year]: is the column/element in array.
        sumy = (index[state].sum(axis=0)[year-(dataStartColumn-1)] + float(data[row][year])) #axis=0 means to sum up columns of dictionary matrix.
        index[state] [0][year-5] = sumy 
    # 4) AVERAGING INDEX OF EACH STATE FOR THAT YEAR - Now that i have sum index of each state and number of counties in each state, can loop through and get the averages
    for i in index:
        index[i] [0][year-(dataStartColumn-1)] = round(index[i] [0][year-(dataStartColumn-1)]  / index[i] [0][0], 7)
    sumy = 0.0



#PRINTING THE DATA AND RINTING IT AS IF ITS A CSV FILE: from original data columns 0-5, print from data matrix. from 6
# Printing the title row:
print("\nNumber of states in dictionary: "+str(len(index))+"\n")
print()
title = ""
for col in range(0,dataEndColumn):
    # Will not print columns: FIPS	FID	   County  (state)	Latitude	Longitude	NDI Median	NDI (column numbers: 0,1,2,4,5,6) 
    # Will also skip 'District of Columbia' and 'Virginia' because they have very large NDI which messes up the grap    if col <= 2 or col == 4 or col == 5 or col == 6 or col==7 : #columns 0 to 2,4,and 5 dont make sense if we are reducing to only states, so don't print these columns
    if col <3 or (col >= 4 and col <=7): #skip those unimportant columns
        continue
    title+=data[0][col]
    if (col < dataEndColumn-1):
        title+=","
print(title+"\n")

# make an array for the states in the dictionary (had troble accesing the keys in the dictionary)
stateList = []
for state in index:
    stateList.append(state)

#Printing the actual datas now
s = ""
for row in range(0,len(index.keys())):
    state = stateList[row]
    # Will not print columns: FIPS	FID	   County	Latitude	Longitude	NDI Median	NDI (column numbers: 0,1,2,4,5,6) 
    # Will also skip 'District of Columbia' and 'Virginia' because they have very large NDI which messes up the grap
    if state == 'District of Columbia' or state == 'Virginia': #skip those states that meses up data
        continue
    for col in range(0, dataEndColumn):
        if col <3 or (col >= 4 and col <=7):  #skip those unimportant columns
            continue
        # Printing the original columns from column 0 to 5
        if col < dataStartColumn: 
            if col == stateColumn:
                s+=state
            else:
                s+=str(data[row][col])  
        else:
            s+=str(index[state] [0][col-(dataStartColumn-1)])
        # Add the , at the end of each element for it to be csv
        if (col < dataEndColumn-1):
            s+=","
    print(s)
    s=""
























# data =  [
#     ['Alabama', '0.2', '2'], 
#     ['Alabama', '0.2', '2'], 
#     ['Alabama', '0.3', '2'], 
#     ['Alabama', '0.4', '2'], 
#     ['A',       '0.4', '3']
# ]# [alambama: 1.1/4, 8.0/4]


#################################append float arrays###########################################
# import numpy as np

# #TEMP1 = [22.4, 14.4, 12.3]
# temperatures = []
# floatTemperatures = np.array(temperatures, dtype = np.float32)
 
# floatTemperatures = np.append(floatTemperatures, 3.0)
# floatTemperatures = np.append(floatTemperatures, 4.0)
# print(floatTemperatures[0])





# #works
# index = dict() # this is an dictionary that has key-value pairs to automatically group all average years for the state
# stateCount = dict()
# sumYr = 0.0    
# count = 0      # counting number of counties in the state so i can use to to find average

# import numpy as np # will use t his to make an array of floats. this array is the value of each state key. The aray hold the index averages for all years
# sumYrTemp = []     # apprantly i need to make this empty array thta i dont even use
# sumYrArray = np.array(sumYrTemp, dtype = np.float32)  # this will be the float array i will append stuff to


# for year in range(1,3):     # looping through year column
#     for row in range(0,4):  # looping through counties rows
#         sumYr = round(sumYr + float(data[row][year]), 8) #***change to 8 or 9. adding the indexes of the current year
#     sumYrArray = np.append(sumYrArray, sumYr) # adding the sum of the index into the float array
#     index[data[row][0]] = sumYrArray
#     sumYr=0
# #print alabama stuff
# for i in range(0,len(index['Alabama'])):
#     print(index['Alabama'][i])



##works
# index = dict()
# sumYr = 0.0
# for year in range(1,3): 
#     for row in range(0,4):
#         sumYr = (round(sumYr + float(data[row][year]), 8))
#     index[data[row][0]] = sumYr
#     print(index[data[row][0]])
#     sumYr=0





# for country in Countries:
#   # For each country from the list check to see whether it is in the dictionary Capitals
#     if country in Capitals:
#         print('The capital of ' + country + ' is ' + Capitals[country])
#     else:
#         print('The capital of ' + country + ' is unknown')