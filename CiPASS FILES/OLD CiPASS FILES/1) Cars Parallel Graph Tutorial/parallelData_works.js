//Mahmudul Hasan
/*EXPLAINING COLORING CODE:
    * Section C will call the color(colorData) method. We will be passing in the colorData method -> this will call the colorCars() function obviously.
    * colorCars() will go through each row in that column i specified ('economy (mpg)' in this case). The economy column will have a range of numbers from 0 to 50, wil will scale it to a range of colors.
    * and it will then call zcolorscale() with each row as the paramenter mapping a number range to a color range
*/
/* ERRORS:
    * importing the csv file with d3.csv() function causes problems in chrome where it keeps showing errors.
        chrome doesnt work unless u run it in a live server (download live server extension on vs code) 
        or u can just open up firefox.
*/

// SECTION A: GETTING DATA SET FROM CSV:
d3.csv('cars.csv', function(data) //so if you run this on chrome without a live server, it shows errors. If u run it with a live server, it works. also works on firefox
{
    // SECTION B: SETTING COLOR OF EACH ROW/CAR from the economy (1st) column of the data:
    var zcolorscale = d3.scale.linear()
        .domain([0,50]) //what is the range of cars to color? 
        .range(["steelblue","purple"]) //range of colors?
        .interpolate(d3.interpolateLab);
    var colorCars = function(d){
        //some of the values of the 'economy (mpg)' column are undefined so im making them 0, idk if this wil work
        if (d['economy (mpg)'] == undefined){
            d['economy (mpg)'] = 0;    
        }
        return zcolorscale(d['economy (mpg)']); //im picking the 1st column (this is the economy column)
    }

    // SECTON C: CREATING PARALLEL GRAPH:
    var pc = d3.parcoords()("#example")
        .data(data)    //importing the data    
        .margin({ top: 50, left: 0, bottom: 30, right: 0 })
        .color(colorCars) //set color of the graph, can can a color value here. but i set a fucntion to color all cars
        .alpha(0.3)    //lightness/density of lines?
         //.composite("lighter") //cant use with brushmode - overlapping graphs are lighter
        .render()      //draws the lines in the graph    
        .createAxes()  //adds the axes 
        .reorderable() //move axis 
        .brushMode("1D-axes")  //filter out graphs
        .interactive();


        //CODE FROM BRISHING:
        // var explore_count = 0;
        // var exploring = {};
        // var explore_start = false;
        // pc.svg
        //   .selectAll(".dimension")
        //   .style("cursor", "pointer")
        //   .on("click", function(d) {
        //     exploring[d] = d in exploring ? false : true;
        //     event.preventDefault();
        //     if (exploring[d]) d3.timer(explore(d,explore_count));
        //   });

        
        // function explore(dimension,count) {
        //     if (!explore_start) {
        //       explore_start = true;
        //       d3.timer(pc.brush);
        //     }
        //     var speed = (Math.round(Math.random()) ? 1 : -1) * (Math.random()+0.5);
        //     return function(t) {
        //       if (!exploring[dimension]) return true;
        //       var domain = pc.yscale[dimension].domain();
        //       var width = (domain[1] - domain[0])/4;
        
        //       var center = width*1.5*(1+Math.sin(speed*t/1200)) + domain[0];
        
        //       pc.yscale[dimension].brush.extent([
        //         d3.max([center-width*0.01, domain[0]-width/400]),
        //         d3.min([center+width*1.01, domain[1]+width/100])
        //       ])(pc.g()
        //           .filter(function(d) {
        //             return d == dimension;
        //           })
        //       );
        //     };
        //   };
});