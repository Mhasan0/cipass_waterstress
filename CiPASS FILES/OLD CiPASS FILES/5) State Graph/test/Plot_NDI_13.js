//Mahmudul Hasan
/* ERRORS:
    * importing the csv file with d3.csv() function causes problems in chrome where it keeps showing errors.
        chrome doesnt work unless u run it in a live server (download live server extension on vs code) 
        or u can just open up firefox.
*/
var t = 0
// SECTION A: GETTING DATA SET FROM CSV:
//d3.csv('13_NDI_Normalized_RidExtremes.csv', function(data) //so if you run this on chrome without a live server, it shows errors. If u run it with a live server, it works. also works on firefox
d3.csv('13_NDI_Normalized_Pattern1.csv', function(data) //so if you run this on chrome without a live server, it shows errors. If u run it with a live server, it works. also works on firefox
{

    //console.log(data)
    // SECTION B3: SETTING COLOR OF EACH ROW/CAR from the state (1st) column of the data (colorIt() passes me the row elements one at a time and this function colors it):
    var zcolorscale = d3.scale.linear()
        .domain([1,8,16,21,32,40]) //what is the range of cars to color? 
        .range(["green", "blue", "violet", "red"]) //range of colors?
        .interpolate(d3.interpolateLab); 
    // SECTION B2:THIS FUNCTION WILL LOOP THROUGHT THE ROWS OF THE COLUMN I CHOOSE AND WILL KEEP PASSING IT TO zcolorscale():
    var t = 0
    var colorIt = function(d){
        column = "FID-State-County"
        t+=1
        console.log(d[column])
        return zcolorscale(t);  //1 to 91
    }


    // SECTON B1: settign colors and margins:
    var parcoords = d3.parcoords()("#myPlot")//("#example")  
        // .dimensions(["Survived", "Sex", "Age", "Class"])
        .margin({ top:20, left: 190, bottom: 20, right: 0})
        .color(colorIt) //set color of the graph, can can a color value here. but i set a fucntion to color all cars
        .alpha(0.7)    //lightness/density of lines?
        //.attr("transform", "rotate(90)")
        //.range([h-margin, 0])
        //.attr("transform", "translate(" + margin.left + "," + margin.top + ")"+ "rotate(270, "+width/2 + "," + height/2 + ")")
    // SECTON C: CREATING PARALLEL GRAPH:
        .data(data)    //importing the data
         //.composite("lighter") //cant use with brushmode - overlapping graphs are lighter
        .render()      //draws the lines in the graph    
        .createAxes()  //adds the axes 
        .reorderable() //move axis 
        .brushMode("1D-axes")  //filter out graphs
        .interactive();


       
});
     