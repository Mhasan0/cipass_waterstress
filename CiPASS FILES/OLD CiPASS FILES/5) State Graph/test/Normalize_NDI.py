# STEP 3: finding patterns using same code as stpe 2
# #GOAL: NORMALIZE each column to see patterns better: X - Xmin / Xmax - Xmin
# Taking the 13 year extreme water stress data from Akash and getting rid of all counties that is only 0 for all years (0=no stress, 1=high stress for that county that yr)
# 13_NDI_ReducedMERGE and 13_NDI_ReducedMERGE_RidExtremes to 13_NDI_Normalized and 13_NDI_Normalized_ridExtremes

# 13_NDI_ReducedMERGE to 13_NDI_Normalized_ridExtremes


import numpy as np # will use this to make an array of floats. this array is the value of each state key. The aray hold the index averages for all years
import csv
f = open('13_NDI_ReducedMERGE.txt') #1) make csv file in a tab delimited .txt
data = []
rowCount = 0
for row in f:
    data_row = row.rstrip().split('\t')  
    data.append(data_row)
    rowCount+=1

stateColumn = 0 #the index of the column the state is is
dataStartColumn = 1 #index where the yearly datas start from 
dataEndColumn = len(data[0]) #end of data column 
columnsZero = dataEndColumn-dataStartColumn+1 #dataEndColumn - dataStartColumn is the pure year data column number + 1 because will add county number to index 0 

# Ridding extreme values
extremes = [
    "1801-Virginia-Franklin City",
    "1309-Virginia-Manassas City",
    "1284-Virginia-Falls Chruch",
    "1665-Virginia-Bedford City",
    "1539-Virginia-Clifton Forge",
    "1675-Virginia-Hopewell",
    "710-Massachusetts-Nantucket",
    "690-Massachusetts-Dukes"
]
# When i printed the normilized data without the extremes, i seen a few patterns. Instead of going into the csv and find the county and state (will tak elong time), will just record the fids
# if the FID is 3 digits, will end it with a '-' because in the csv a 3 digit is folowed by a "-" same as with a 4 digit. so now we can look at both 3 and 4 digit FIDs
pattern1_EXCLUDE = [ 
    "1868",
    "840-",
    "1096",
    "3087",
    "3070",
    "1183",
    "576-",
    "1420",
    "1256",
    "704-",
    "504-",
    "464-",
    "1565",
    "1188",
    "1697",
    "2396",
    "956",
    "1592",
    "890-", #somwhat
    "2829", #somewhat
    "2925",
    "1483",
    "1152",
    "3084",
    "109-",
    "707-",
    "104-",
    "638-",
    "634-",
    "476-",
    "3008",
    "1363",
    "644-",
    "914-",
    "673-",
    "2889",
    "2876",
    "1936",
    "140-",
    "2907",
    "3035",
    "2151",
    "1659", #maybe not
    "1253", #maybe not
    "2889",
    "3033",
    "1900",#p3
    "2039",#p3?
    "2050", #p3?
    "1939",
    "1190",
    "1504",
    "969"
]
# "",
# "",
# "",
# "",
# "",
# "",
# "",
# "",
# "",
# "",
# "",
# "",
# ""
pattern2 = [
    "478-",
    "515-",
    "398-",
    "720-",
    "956-",
    "621-",
    "1063",
    "683-", #maybe?
    "1969", #maybe
    "",
    "",
    "",
    ""
]

# For normilizing: making a max/min array to keep track of all max/min vla of each year
maxy = []
miny = []
maxy.append(0.0) #unimportant first space
miny.append(0.0)

# Fing max and min of each column
for col in range(dataStartColumn, dataEndColumn): 
    maxy.append(0.0) #initial val to check for this column
    miny.append(10000000.0)
    for row in range(1,rowCount): 
        # if data[row][0] in extremes: #**DONT NORMILIZE WITHOUT MAX VALUES
        #     continue
        if float(data[row][col]) >= maxy[col]:
            maxy[col] = float(data[row][col])
        if float(data[row][col]) < miny[col]:
            miny[col] = float(data[row][col])

# use the max and min values to normilize data
for col in range(dataStartColumn, dataEndColumn): 
    for row in range(1,rowCount):
        if data[row][0] in extremes:
            continue
        data[row][col] = round((float(data[row][col]) - miny[col]) / (maxy[col] - miny[col]), 8)


#PRINTING TITLE
title = ""
for col in range(0,dataEndColumn):
    title+=str(data[0][col])
    if (col < dataEndColumn-1):
        title+=","
print(str(title))

s = "" #data string
p = "" #pattern1 in counties, state format
for row in range(dataStartColumn,rowCount): 
    # Exclusing Extreme Values from printing
    if data[row][0] in extremes:
        continue
    # Exclusing Counties thats not in Pattern1
    fid = data[row][0][0:4]
    if (fid in pattern1_EXCLUDE) or (fid in pattern2):
        #print("EXCLUDED "+data[row][0]+ " Because its not in pattern1")
        continue
    # Printing Data
    for col in range(0,dataEndColumn):
        # printing pattern1 in county,state format:
        if col == 0:
            split = data[row][0].split('-')
            p+=split[1]+","+split[2]+","+split[0]+"\n"
        # acutal dta printing:
        s += str(data[row][col])
        if (col < dataEndColumn-1):             
            s+=","
    s+="\n"
print(s)
print("\n"+p)

