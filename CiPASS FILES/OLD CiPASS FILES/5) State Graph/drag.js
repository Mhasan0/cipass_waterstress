/* Strategy: console.log(clicked[0]["State-County-FID"]) 	
js prints what im hovering on. will add longitude and latidue data, will ignore those two columns when printing uding d3. when i hover, will get that data
*/


var color_set = d3.scale.linear()
	//domain is the range of values it will mapp the colors to. i pass the domain after i read the values
	.range(["red", "green", "blue", "yellow", "black", "orange", "purple"])
	//.range(["#3182bd", "#f33",  '#FF33FF']); 	//COLOR RANGE IS BLUE TO RED
	// .range(['#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6', 
	// 	  '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
	// 	  '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A', 
	// 	  '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',]);
		//   '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC', 
		//   '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
		//   '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680', 
		//   '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
		//   '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3', 
		//   '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF']);


// load default chart
d3.csv("13_NDI_Normalized_Pattern1_f.csv", function(data){

    // GET FIRST COLUMN DATA (aka get the state names)
    var firstCol = data.map(function(d){return d3.values(d)[0]});			//***firstCol=the first column...aka states column

    // LOOP THROUGH FIRST COLUMN - Need to adject left margin so find out the longest text in the first column 
    var textLength = 0;
    firstCol.forEach(function(d){
		if (d.length > textLength)
			textLength = d.length;
    });
        
    // Get parallel coordinates
    graph = d3.parcoords()('#myPlot')
        .data(data)
		.margin({ top: 30, left: 5*textLength, bottom: 40, right: 0 })
		.alpha(3)			   // lightness/density of lines
		.hideAxis(["colors"])
		.mode("queue")
		.rate(5)
		.render()			   // draw the line son the graph
		.brushMode("1D-axes")  // enable brushing mode to filter out lines
		.interactive();

    // Add instruction text
	var instructions = "Drag on axis to create brush. Click axis to clear brush. Click a label to color data based on the values of that axis. Hover on each line to highlight it."
    d3.select("#myPlot svg").append("text")
        .text(instructions)
        .attr("text-anchor", "middle")
		// .attr("text-decoration", "overline")
        .attr("transform", "translate(" + graph.width()/2 + "," + (graph.height()) + ")");


//*1	// set the initial colorGraph based on the 2rd column (year 19)
    colorGraph(d3.keys(data[0])[14]); 	//8 Im going to color via the 2nd column for now: the 1952 column
   
	// click label to activate colorGraph
    graph.svg.selectAll(".column")
        .on("click", colorGraph)
        .selectAll(".label")
        .style("font-size", "14px"); // change font sizes of selected lable

    //add hover event
    d3.select("#myPlot svg")
        .on("mousemove", function() {
            var mousePosition = d3.mouse(this);			    
			highlightLineOnClick(mousePosition, true); //true will also add tooltip
        })
        .on("mouseout", function(){
            cleanTooltip();
            graph.unhighlight();
        });
        
});


// update color and font weight of chart based on axis selection
// modified from here: https://syntagmatic.github.io/parallel-coordinates/
//*2
function colorGraph(column)  //*i passed the 2nd columns and so am colorGraph base don it: 1952
{ 
	// changing the font weight
	graph.svg.selectAll(".column")
		.style("font-weight", "normal")
		.filter(function(d) { return d == column; })
			.style("font-weight", "bold");
			

	//*2 Find the domain of the color values, will pass it next		
	// var statesDict= new Object(); 	//Will color by states so am making dictionary object to assign a state a number. will pass that number range to color.
	// var stateCount = 0.0											//GETTING VALUES AND SETTING COLROS
	var values = graph.data().map(function(d){
		// let split = d[column].split("-"); 	// split the state cell into its FID, State, and Columns sectios n so that i cna easily use it
		// if (!(split[1] in statesDict)){		// if the state isnt int the dictionary, assign the state a number 
		// 	stateCount+=1.0
		// 	statesDict[split[1]] = stateCount
		// }
		// console.log(d[column]+"   "+split[1]+"   "+statesDict[split[1]]);
		// return parseFloat(parseInt(statesDict[split[1]]))
		console.log(d[column])
		return parseFloat(d[column])
	}); 
	
//*3 FINALLY PASSING DOMAIN VALUES I FOUND AND SETTING THEM
	color_set.domain([d3.min(values), d3.max(values)]); //*passes values from the column i picked, this one is the 2nd column: 1952 so range [0, 0.16]
	// Now i need to actually change the colors int he graph
	graph.color(function(d){
		return color_set([d[column]])
	}).render();
};		





// Add highlight for every line on click
function getCentroids(data)
{
	// this function returns centroid points for data. I had to change the source
	// for parallelcoordinates and make compute_centroids public.
	// I assume this should be already somewhere in graph and I don't need to recalculate it
	// but I couldn't find it so I just wrote this for now
	var margins = graph.margin();
	var graphCentPts = [];
	
	data.forEach(function(d){
		
		var initCenPts = graph.compute_centroids(d).filter(function(d, i){return i%2==0;});
		
		// move points based on margins
		var cenPts = initCenPts.map(function(d){
			return [d[0] + margins["left"], d[1]+ margins["top"]]; 
		});

		graphCentPts.push(cenPts);
	});

	return graphCentPts;
}

function getActiveData(){
	// I'm pretty sure this data is already somewhere in graph
	if (graph.brushed()!=false) 
		return graph.brushed(); //*** false=shows point when hovering 
	return graph.data();
}


//*** i think this calculates if im on the line
function isOnLine(startPt, endPt, testPt, tol){
	// check if test point is close enough to a line
	// between startPt and endPt. close enough means smaller than tolerance
	var x0 = testPt[0];
	var	y0 = testPt[1];
	var x1 = startPt[0];
	var	y1 = startPt[1];
	var x2 = endPt[0];
	var	y2 = endPt[1];
	var Dx = x2 - x1;
	var Dy = y2 - y1;
	var delta = Math.abs(Dy*x0 - Dx*y0 - x1*y2+x2*y1)/Math.sqrt(Math.pow(Dx, 2) + Math.pow(Dy, 2)); 
	//console.log(delta);
	if (delta <= tol) return true;
	return false;
}

function findAxes(testPt, cenPts){
	// finds between which two axis the mouse is
	var x = testPt[0];
	var y = testPt[1];

	// make sure it is inside the range of x
	if (cenPts[0][0] > x) return false;
	if (cenPts[cenPts.length-1][0] < x) return false;

	// find between which segment the point is
	for (var i=0; i<cenPts.length; i++){
		if (cenPts[i][0] > x) return i;
	}
}

function cleanTooltip(){
	// removes any object under #tooltip is
	graph.svg.selectAll("#tooltip")
    	.remove();
}

function addTooltip(clicked, clickedCenPts){
	
	// sdd tooltip to multiple clicked lines
    var clickedDataSet = [];
    var margins = graph.margin()

    // get all the values into a single list
    // I'm pretty sure there is a better way to write this is Javascript
    for (var i=0; i<clicked.length; i++){
    	for (var j=0; j<clickedCenPts[i].length; j++){
    		var text = d3.values(clicked[i])[j];
  			// not clean at all!
  			var x = clickedCenPts[i][j][0] - margins.left;
  			var y = clickedCenPts[i][j][1] - margins.top;
  			clickedDataSet.push([x, y, text]);
		}
	};

	// add rectangles
	var fontSize = 14;
	var padding = 2;
	var rectHeight = fontSize + 2 * padding; //based on font size

	graph.svg.selectAll("rect[id='tooltip']")
        	.data(clickedDataSet).enter()
        	.append("rect")
        	.attr("x", function(d) { return d[0] - d[2].length * 5;})
			.attr("y", function(d) { return d[1] - rectHeight + 2 * padding; })
			.attr("rx", "2")
			.attr("ry", "2")
			.attr("id", "tooltip")
			.attr("fill", "grey")
			.attr("opacity", 0.9)
			.attr("width", function(d){return d[2].length * 10;})
			.attr("height", rectHeight);

	// add text on top of rectangle
	graph.svg.selectAll("text[id='tooltip']")
    	.data(clickedDataSet).enter()
    		.append("text")
			.attr("x", function(d) { return d[0];})
			.attr("y", function(d) { return d[1]; })
			.attr("id", "tooltip")
			.attr("fill", "white")
			.attr("text-anchor", "middle")
			.attr("font-size", fontSize)
        	.text( function (d){ return d[2];})    
}

function getClickedLines(mouseClick){
    var clicked = [];
    var clickedCenPts = [];

	// find which data is activated right now
	var activeData = getActiveData();					//**get info on what i highlighted */

	// find centriod points
	var graphCentPts = getCentroids(activeData);

    if (graphCentPts.length==0) return false;

	// find between which axes the point is
    var axeNum = findAxes(mouseClick, graphCentPts[0]);
    if (!axeNum) return false;
    
    graphCentPts.forEach(function(d, i){
	    if (isOnLine(d[axeNum-1], d[axeNum], mouseClick, 2)){
	    	clicked.push(activeData[i]);
	    	clickedCenPts.push(graphCentPts[i]); 					// for tooltip
	    }
	});

	// console.log(clicked[0]["State-County-FID"]) 					//***getting fid-state-county info over the one selected! 
	// console.log(clickedCenPts) 									//***14x2 matrkx: array of 14 objects/arrays. each obj has 2 elem

	return [clicked, clickedCenPts]
}


function highlightLineOnClick(mouseClick, drawTooltip){
	
	var clicked = [];
    var clickedCenPts = [];
	
	clickedData = getClickedLines(mouseClick);

	if (clickedData && clickedData[0].length!=0){

		clicked = clickedData[0];
    	clickedCenPts = clickedData[1];
		console.log(clicked) 

	    // highlight clicked line
	    graph.highlight(clicked);
		
		if (drawTooltip){
			// clean if anything is there
			cleanTooltip();
	    	// add tooltip
	    	addTooltip(clicked, clickedCenPts);
		}

	}
};
