//Mahmudul Hasan
/*EXPLAINING COLORING CODE:
    * Section C will call the color(colorData) method. We will be passing in the colorData method -> this will call the colorCars() function obviously.
    * colorCars() will go through each row in that column i specified ('economy (mpg)' in this case). The economy column will have a range of numbers from 0 to 50, wil will scale it to a range of colors.
    * and it will then call zcolorscale() with each row as the paramenter mapping a number range to a color range
*/
/* ERRORS:
    * importing the csv file with d3.csv() function causes problems in chrome where it keeps showing errors.
        chrome doesnt work unless u run it in a live server (download live server extension on vs code) 
        or u can just open up firefox.
*/

// SECTION B: SETTING COLOR OF EACH ROW/CAR from the economy (1st) column of the data:
var zcolorscale = d3.scale.linear()
    .domain([0, 0.25, 0.5, 1]) //what is the range of cars to color? 
    .range(["steelblue","purple"]) //range of colors?
    .interpolate(d3.interpolateLab);
var colorCars = function(d){
    //some of the values of the 'economy (mpg)' column are undefined so im making them 0, idk if this wil work
    var column = 'NDI Median'
    if (d[column] == undefined){
        d[column] = 0;    
    }
    return zcolorscale(d[column]); //im picking the 1st column (this is the economy column)
}
// SECTON C part 2: settign colors:
var parcoords = d3.parcoords()("#example")//("#example")  
    .margin({ top:20, left: 10, bottom: 10, right: 10 })
    .color(colorCars) //set color of the graph, can can a color value here. but i set a fucntion to color all cars
    .alpha(0.3)    //lightness/density of lines?

// SECTION A: GETTING DATA SET FROM CSV:
d3.csv('indexTest.csv', function(data) //so if you run this on chrome without a live server, it shows errors. If u run it with a live server, it works. also works on firefox
{

    // SECTON C part 1: CREATING PARALLEL GRAPH:
    parcoords 
        .data(data)    //importing the data    
         //.composite("lighter") //cant use with brushmode - overlapping graphs are lighter
        .render()      //draws the lines in the graph    
        .createAxes()  //adds the axes 
        .reorderable() //move axis 
        .brushMode("1D-axes")  //filter out graphs
        .interactive();
        
        
        // var brushedData;

        // // update data table on brush event
        // parcoords.on("brush", function(d) {
        //   brushedData = d;
        //   d3.select("#grid")
        //     .datum(d.slice(-10).reverse())
        //     .call(grid)
        //     .selectAll(".row")
        //     .on({
        //       "mouseover": function(d) {
        //         d3.select(this).classed("highlighted", true);
        //         var thisLabel = d3.select(".dimension").selectAll(".tick").filter(x => x === d.Sport)[0];
        //         d3.select(".dimension").selectAll(".tick").select("text").style("fill", "#555");
        //         d3.select(thisLabel[0]).select("text").style("fill", "#ddd");
      
        //         parcoords.highlight([d]);
      
        //       },
        //       "mouseout": function() {
        //         d3.select(this).classed("highlighted", MSFIDOCredentialAssertion);
        //         parcoords.unhighlight();
        //         d3.select(".dimension").selectAll(".tick").select("text").style("fill", "#aaa");
        //       }
        //     });
});
      
    //SECTION D: create data table, row hover highlighting
//    // var grid = d3.divgrid();
//     d3.select("#grid")
//         .datum(data.slice(-10).reverse())
//         .call(grid)
//         .selectAll(".row")
//         .on({
//             "mouseover": function(d) {
//                 d3.select(this).classed("highlighted", true);
//                 parcoords.highlight([d]);
//                 var thisLabel = d3.select(".dimension").selectAll(".tick").filter(x => x === d.Sport)[0];
//                 d3.select(".dimension").selectAll(".tick").select("text").style("fill", "#555");
//                 d3.select(thisLabel[0]).select("text").style("fill", "#ddd");
//             },
//             "mouseout": function() {
//                 d3.select(this).classed("highlighted", false);
//                 parcoords.unhighlight();
//                 d3.select(".dimension").selectAll(".tick").select("text").style("fill", "#aaa");
//             }
//         });
//         var brushedData;

        // // update data table on brush event
        // parcoords.on("brush", function(d) {
        //   brushedData = d;
        //   d3.select("#grid")
        //     .datum(d.slice(-10).reverse())
        //     .call(grid)
        //     .selectAll(".row")
        //     .on({
        //       "mouseover": function(d) {
        //         d3.select(this).classed("highlighted", true);
        //         var thisLabel = d3.select(".dimension").selectAll(".tick").filter(x => x === d.Sport)[0];
        //         d3.select(".dimension").selectAll(".tick").select("text").style("fill", "#555");
        //         d3.select(thisLabel[0]).select("text").style("fill", "#ddd");
      
        //         parcoords.highlight([d]);
      
        //       },
        //       "mouseout": function() {
        //         d3.select(this).classed("highlighted", MSFIDOCredentialAssertion);
        //         parcoords.unhighlight();
        //         d3.select(".dimension").selectAll(".tick").select("text").style("fill", "#aaa");
        //       }
        //     });
        // });
      
        // // label interactions
        // d3.select(".dimension").selectAll(".tick")
        //   .on("mouseover", function(d) {
        //     // highlight the label
        //     d3.select(".dimension").selectAll(".tick").select("text").style("fill", "#555");
        //     d3.select(this).select("text").style("fill", "#ddd");
            
        //     // highlight the line
        //     parcoords.highlight(data.filter(x => x.Sport === d));
      
        //     // highlight the row in the table
        //     d3.select("#grid")
        //       .datum(data.filter(x => x.Sport === d))
        //       .call(grid);
      
        //   })
        //   .on("mouseout", function(d) {
        //     // unhighlight the label
        //     d3.select(".dimension").selectAll(".tick").select("text")
        //       .style("fill", "#aaa");
            
        //     // unhighlight the line
        //     parcoords.unhighlight();
      
        //     // unhighlight the row in the table
        //     d3.select("#grid")
        //       .datum(brushedData ? brushedData.slice(-10).reverse() : data.slice(-10).reverse())
        //       .call(grid)
        //       .selectAll(".row")
        //       .on({
        //         "mouseover": function(d) {
        //           d3.select(this).classed("highlighted", true);
      
        //           var thisLabel = d3.select(".dimension").selectAll(".tick").filter(x => x === d.Sport)[0];
        //           d3.select(".dimension").selectAll(".tick").select("text").style("fill", "#555");
        //           d3.select(thisLabel[0]).select("text").style("fill", "#ddd");
      
        //           parcoords.highlight([d]);
      
        //         },
        //         "mouseout": function() {
        //           d3.select(this).classed("highlighted", false);
        //           parcoords.unhighlight();
        //           d3.select(".dimension").selectAll(".tick").select("text").style("fill", "#aaa");
        //         }
        //       });
      
        //   });




