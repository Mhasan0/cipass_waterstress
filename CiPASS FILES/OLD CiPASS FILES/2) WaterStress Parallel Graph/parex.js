var colorScale = d3.scale.linear()
  .domain([14, 20, 26, 32, 38, 44, 50, 56, 62, 68, 73])
  .range(color_maps.RdYlBu["11"].reverse())
  .interpolate(d3.interpolateLab);

var color = function(d) {
  var totalScore = Object.values(d).slice(1).reduce((a, b) => +a + +b);
  return colorScale(totalScore);
};

var parcoords = d3.parcoords()("#example")
  .color(color)
  .alpha(0.25)
  .margin({
    top: 15,
    left: 165,
    right: 0,
    bottom: 15
  });

function reject(obj, keys) {
    return Object.assign({}, ...Object.keys(obj).filter(k => !keys.includes(k)).map(k => ({[k]: obj[k]})))
}

// load csv file and create the chart
var test;
d3.csv('sport_skills.csv', function(data) 
{
  data = data.map(d => reject(d, ["Rank"])); 
  parcoords
    .data(data)
    // .dimensions(dimensions)
    .render()
    // .reorderable()
    .brushMode("1D-axes");  // enable brushing

  // create data table, row hover highlighting
  var grid = d3.divgrid();
  d3.select("#grid")
    .datum(data.slice(-10).reverse())
    .call(grid)
    .selectAll(".row")
    .on({
      "mouseover": function(d) {
        d3.select(this).classed("highlighted", true);
        parcoords.highlight([d]);
        var thisLabel = d3.select(".dimension").selectAll(".tick").filter(x => x === d.Sport)[0];
        d3.select(".dimension").selectAll(".tick").select("text").style("fill", "#555");
        d3.select(thisLabel[0]).select("text").style("fill", "#ddd");
      },
      "mouseout": function() {
        d3.select(this).classed("highlighted", false);
        parcoords.unhighlight();
        d3.select(".dimension").selectAll(".tick").select("text").style("fill", "#aaa");
      }
    });

  var brushedData;

  // update data table on brush event
  parcoords.on("brush", function(d) {
    brushedData = d;
    d3.select("#grid")
      .datum(d.slice(-10).reverse())
      .call(grid)
      .selectAll(".row")
      .on({
        "mouseover": function(d) {
          d3.select(this).classed("highlighted", true);
          var thisLabel = d3.select(".dimension").selectAll(".tick").filter(x => x === d.Sport)[0];
          d3.select(".dimension").selectAll(".tick").select("text").style("fill", "#555");
          d3.select(thisLabel[0]).select("text").style("fill", "#ddd");

          parcoords.highlight([d]);

        },
        "mouseout": function() {
          d3.select(this).classed("highlighted", MSFIDOCredentialAssertion);
          parcoords.unhighlight();
          d3.select(".dimension").selectAll(".tick").select("text").style("fill", "#aaa");
        }
      });
  });

  // label interactions
  d3.select(".dimension").selectAll(".tick")
    .on("mouseover", function(d) {
      // highlight the label
      d3.select(".dimension").selectAll(".tick").select("text").style("fill", "#555");
      d3.select(this).select("text").style("fill", "#ddd");
      
      // highlight the line
      parcoords.highlight(data.filter(x => x.Sport === d));

      // highlight the row in the table
      d3.select("#grid")
        .datum(data.filter(x => x.Sport === d))
        .call(grid);

    })
    .on("mouseout", function(d) {
      // unhighlight the label
      d3.select(".dimension").selectAll(".tick").select("text")
        .style("fill", "#aaa");
      
      // unhighlight the line
      parcoords.unhighlight();

      // unhighlight the row in the table
      d3.select("#grid")
        .datum(brushedData ? brushedData.slice(-10).reverse() : data.slice(-10).reverse())
        .call(grid)
        .selectAll(".row")
        .on({
          "mouseover": function(d) {
            d3.select(this).classed("highlighted", true);

            var thisLabel = d3.select(".dimension").selectAll(".tick").filter(x => x === d.Sport)[0];
            d3.select(".dimension").selectAll(".tick").select("text").style("fill", "#555");
            d3.select(thisLabel[0]).select("text").style("fill", "#ddd");

            parcoords.highlight([d]);

          },
          "mouseout": function() {
            d3.select(this).classed("highlighted", false);
            parcoords.unhighlight();
            d3.select(".dimension").selectAll(".tick").select("text").style("fill", "#aaa");
          }
        });

    });

});