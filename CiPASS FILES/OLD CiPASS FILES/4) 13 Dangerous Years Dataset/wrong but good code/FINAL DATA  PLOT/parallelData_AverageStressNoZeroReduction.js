//Mahmudul Hasan
/* ERRORS:
    * importing the csv file with d3.csv() function causes problems in chrome where it keeps showing errors.
        chrome doesnt work unless u run it in a live server (download live server extension on vs code) 
        or u can just open up firefox.
*/
var t = 0
d3.csv('DangerousYears_3_1to3_AveragedALLStress.csv', function(data) //so if you run this on chrome without a live server, it shows errors. If u run it with a live server, it works. also works on firefox
{
    var max = 0
    var min = Infinity
    // SECTION B2: SETTING COLOR OF EACH ROW/CAR from the state (1st) column of the data (colorIt() passes me the row elements one at a time and this function colors it):
    var zcolorscale = d3.scale.linear()
        .domain([399,600, 800, 1000, 1200, 1384])//what is the range of cars to color? 
        .range(["green", "slateblue", "blue", "violet", "orange", "red"]) //range of colors?
        .interpolate(d3.interpolateLab); 
    // SECTION B1:THIS FUNCTION WILL LOOP THROUGHT THE ROWS OF THE COLUMN I CHOOSE AND WILL KEEP PASSING IT TO zcolorscale():
    var colorIt = function(d){
        // MY OLD WAY: ASSIGN A STATE ITS UNIQUE ID USING ASCII TABLE
            var t = 0
            var column = 'State'
            // This the portion below if only the element is a letter
            if ( (d[column][0].charCodeAt() >= 65 && d[column][0].charCodeAt() <= 90)  || (d[column][0].charCodeAt() >= 97 && d[column][0].charCodeAt() <= 122)  ){ //only run this if d[column] is going to be string/letter values
                for (i=0; i< d[column].length; ++i){ //assigning a state a number so i can color it. looping through the state name and addignthe ASCII number of each character so that there will be no duplicate colors
                   t = t + d[column][i].charCodeAt()
                }
                if (t >= max){
                    max = t;
                }
                if (t <= min){
                    min = t;
                }
                //console.log(d[column] + "   " +t)
            }
            console.log("max: "+ max);
            console.log("min: " + min);
       // NVM: BETTER WAY: JUST ASSIGN A STATE A NUMBER BY INCREMENTING T EACH TIME. 47 states to t range: 1 to 47
        // t+=1
        // console.log(t)
        return zcolorscale(t); 
    }



    // SECTON C part 2: settign colors and margins:
    var parcoords = d3.parcoords()("#example")//("#example")  
        // .dimensions(["Survived", "Sex", "Age", "Class"])
        .margin({ top:20, left: 10, bottom: 10, right: 10 })
        .color(colorIt) //set color of the graph, can can a color value here. but i set a fucntion to color all cars
        .alpha(0.7)    //lightness/density of lines?
        //.range([h-margin, 0])
        //.attr("transform", "translate(" + margin.left + "," + margin.top + ")"+ "rotate(270, "+width/2 + "," + height/2 + ")")
    // SECTON C part 1: CREATING PARALLEL GRAPH:
        .data(data)    //importing the data
         //.composite("lighter") //cant use with brushmode - overlapping graphs are lighter
        .render()      //draws the lines in the graph    
        .createAxes()  //adds the axes 
        .reorderable() //move axis 
        .brushMode("1D-axes")  //filter out graphs
        .interactive();

        
});
     