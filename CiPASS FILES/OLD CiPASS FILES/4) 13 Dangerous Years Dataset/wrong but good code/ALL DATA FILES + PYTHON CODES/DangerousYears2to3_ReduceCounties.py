# IDK what the professor wants so im going to find the average 0 or 1 score of all the endangered counties of a state
# Taking the data without the unaffected counties and averaging them to only show states
# Basically: using 2.txt to make 3.txt
# MAY NEED TO REDO TURNING 1.txt directly into 3.txt because i got rid of counties (which will affect my average!)
#redone and working

import numpy as np # will use this to make an array of floats. this array is the value of each state key. The aray hold the index averages for all years
import csv
f = open('DangerousYears_2.txt') #1) make csv file in a tab delimited .txt
# 0) Put in data matrix:
data = []
rowCount = 0
for row in f:
    data_row = row.rstrip().split('\t')  
    data.append(data_row)
    rowCount+=1
stateColumn = 0 #the index of the column the state is is
dataStartColumn = 2 #index where the yearly datas start from 
dataEndColumn = len(data[0]) #end of data column 
columnsZero = dataEndColumn-dataStartColumn+1 #dataEndColumn - dataStartColumn is the pure year data column number + 1 because will add county number to index 0 
index = dict() # this is an dictionary that has key-value pairs to automatically group all average years for the state    

for year in range(dataStartColumn,dataEndColumn): # looping through year column
    for row in range(1,rowCount):                 # looping through counties rows
        state = data[row][stateColumn] 
        if state not in index:
            index[state] = np.zeros((1,columnsZero)) #***MAKING ARRAY OF ZEROES for each state to store the yearly index of each state
        if year == dataStartColumn: 
            index[state] [0][0] = index[state] [0][0] + 1 
            #print(data[row][1]+"    "+str(index["Nebraska"] [0][0]))
        sumy = (index[state].sum(axis=0)[year-(dataStartColumn-1)] +  float(data[row][year])) #axis=0 means to sum up columns of dictionary matrix.
        index[state] [0][year-(dataStartColumn-1)] = sumy 
    for i in index:
        index[i] [0][year-(dataStartColumn-1)] = round(index[i] [0][year-(dataStartColumn-1)]  / index[i] [0][0], 7)
    sumy = 0.0


#PRINTING THE DATA AND RINTING IT AS IF ITS A CSV FILE: from original data columns 0-5, print from data matrix. from 6
# Printing the title row:
print("\nNumber of states in dictionary: "+str(len(index))+"\n")
print()
title = ""
for col in range(0,dataEndColumn):
    if col == 1: #skip county name
        continue
    title+=data[0][col]
    if (col < dataEndColumn-1):
        title+=","
print(title+"\n")

# make an array for the states in the dictionary (had troble accesing the keys in the dictionary)
stateList = []
for state in index:
    stateList.append(state)

#Printing the actual datas now
s = ""
for row in range(0,len(index.keys())):
    state = stateList[row]
    for col in range(0, dataEndColumn):
        if col ==1:
            continue
        # Printing the original columns from column 0 to 5
        if col < dataStartColumn: 
            if col == stateColumn:
                s+=state
            else:
                s+=str(data[row][col])  
        else:
            s+=str(index[state] [0][col-(dataStartColumn-1)])
        # Add the , at the end of each element for it to be csv
        if (col < dataEndColumn-1):
            s+=","
    print(s)
    s=""


