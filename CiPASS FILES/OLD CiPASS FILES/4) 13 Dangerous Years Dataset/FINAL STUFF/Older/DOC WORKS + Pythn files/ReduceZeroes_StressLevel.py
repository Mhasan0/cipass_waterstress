#GOAL: take the 13 dangerous years data and get rid of rows that has 0 stress in all 13 years (so that only the endangered counties exist)
# Taking the 13 year extreme water stress data from Akash and getting rid of all counties that is only 0 for all years (0=no stress, 1=high stress for that county that yr)


import numpy as np # will use this to make an array of floats. this array is the value of each state key. The aray hold the index averages for all years
import csv
f = open('13_Stress_Level.txt') #1) make csv file in a tab delimited .txt
data = []
rowCount = 0
for row in f:
    data_row = row.rstrip().split('\t')  
    data.append(data_row)
    rowCount+=1

stateColumn = 0 #the index of the column the state is is
dataStartColumn = 1 #index where the yearly datas start from 
dataEndColumn = len(data[0]) #end of data column 
columnsZero = dataEndColumn-dataStartColumn+1 #dataEndColumn - dataStartColumn is the pure year data column number + 1 because will add county number to index 0 
printTheseRows = []
printTheseRows.append(0) #to print the title row

for row in range(1,rowCount): #forget title row
    rowSum = 0
    for year in range(dataStartColumn,dataEndColumn):
        rowSum = rowSum + int(data[row][year])
    if (rowSum != 0):
        printTheseRows.append(row)
print(printTheseRows)
print("\n")

# #PRINTING TITLE
# title = ""
# for col in range(0,dataEndColumn):
#     title+=data[0][col]
#     if (col < dataEndColumn-1):
#         title+=","
# print(title+"\n")

s = ""
for rowNumber in printTheseRows: 
    for col in range(0,dataEndColumn):
        s += data[rowNumber][col]
        if (col < dataEndColumn-1):             
            s+=","
    s+="\n"
print(s)
        
