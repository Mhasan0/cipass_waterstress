# STEP 2:
# #GOAL: NORMALIZE each column to see patterns better: X - Xmin / Xmax - Xmin
# Taking the 13 year extreme water stress data from Akash and getting rid of all counties that is only 0 for all years (0=no stress, 1=high stress for that county that yr)
# 13_NDI_ReducedMERGE and 13_NDI_ReducedMERGE_RidExtremes to 13_NDI_Normalized and 13_NDI_Normalized_ridExtremes

# 13_NDI_ReducedMERGE to 13_NDI_Normalized_ridExtremes


import numpy as np # will use this to make an array of floats. this array is the value of each state key. The aray hold the index averages for all years
import csv
f = open('13_NDI_ReducedMERGE.txt') #1) make csv file in a tab delimited .txt
data = []
rowCount = 0
for row in f:
    data_row = row.rstrip().split('\t')  
    data.append(data_row)
    rowCount+=1

stateColumn = 0 #the index of the column the state is is
dataStartColumn = 1 #index where the yearly datas start from 
dataEndColumn = len(data[0]) #end of data column 
columnsZero = dataEndColumn-dataStartColumn+1 #dataEndColumn - dataStartColumn is the pure year data column number + 1 because will add county number to index 0 

# Ridding extreme values
extremes = [
    "1801-Virginia-Franklin City",
    "1309-Virginia-Manassas City",
    "1284-Virginia-Falls Chruch",
    "1665-Virginia-Bedford City",
    "1539-Virginia-Clifton Forge"
]

# For normilizing: making a max/min array to keep track of all max/min vla of each year
maxy = []
miny = []
maxy.append(0.0) #unimportant first space
miny.append(0.0)

# Fing max and min of each column
for col in range(dataStartColumn, dataEndColumn): 
    maxy.append(0.0) #initial val to check for this column
    miny.append(10000000.0)
    for row in range(1,rowCount): 
        if data[row][0] in extremes:
            print("EXTREME "+ data[row][col]+ " FOUND, SKIP")
            continue
        if float(data[row][col]) >= maxy[col]:
            maxy[col] = float(data[row][col])
        if float(data[row][col]) < miny[col]:
            miny[col] = float(data[row][col])

# use the may and min values to normilize data
for col in range(dataStartColumn, dataEndColumn): 
    for row in range(1,rowCount):
        if data[row][0] in extremes:
            continue
        data[row][col] = round((float(data[row][col]) - miny[col]) / (maxy[col] - miny[col]), 8)


#PRINTING TITLE
title = ""
for col in range(0,dataEndColumn):
    if col==1 or col ==2:
        continue
    title+=str(data[0][col])
    if (col < dataEndColumn-1):
        title+=","
print(str(title)+"\n")

s = ""
for rowNumber in range(1,rowCount): 
    if data[rowNumber][0] in extremes:
        continue
    for col in range(0,dataEndColumn):
        if col==1 or col ==2:
            continue
        s += str(data[rowNumber][col])
        if (col < dataEndColumn-1):             
            s+=","
    s+="\n"
print(s)
        
