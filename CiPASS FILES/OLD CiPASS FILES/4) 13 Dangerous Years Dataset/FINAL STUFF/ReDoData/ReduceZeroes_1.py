# STEP 1 and 2:
# GOAL: take the 13 dangerous years data and get rid of rows that has 0 stress in all 13 years (so that only the endangered counties exist)
# Taking the 13 year extreme water stress data from Akash and getting rid of all counties that is only 0 for all years (0=no stress, 1=high stress for that county that yr)

#13_origin to 13_2_ReduceZeroes

import numpy as np # will use this to make an array of floats. this array is the value of each state key. The aray hold the index averages for all years
import csv
f = open('13_0_Original.txt') #1) make csv file in a tab delimited .txt
data = []
rowCount = 0
for row in f:
    data_row = row.rstrip().split('\t')  
    data.append(data_row)
    rowCount+=1

stateColumn = 2 #the index of the column the state is is
dataStartColumn = 4 #index where the yearly datas start from 
dataEndColumn = len(data[0]) #end of data column 
columnsZero = dataEndColumn-dataStartColumn+1 #dataEndColumn - dataStartColumn is the pure year data column number + 1 because will add county number to index 0 
printTheseRows = []

# Merging FID, County, and State Columns into one
# for row in range(0,rowCount):
#     data[row][0] = data[row][0] + "-"+data[row][2]+"-"+data[row][1]

# Figuring out which rows to print
for row in range(1,rowCount): #forget title row
    count = 0
    for col in range(dataStartColumn, dataEndColumn):
        if float(data[row][col]) < 1.0:
            count+=1
        data[row][col] = round(float(data[row][col]), 7)
        #print(data[row][2]+"   "+data[row][1]+"   "+str(count)+"   "+str(data[row][col]))
    if count < 13: #***13 zero counts means that the entire row is less than 1 NDI or 0 stress level, so if there is at least one non-zero stress level, will add to rows to print
        printTheseRows.append(row)

#PRINTING TITLE
title = ""
for col in range(0,dataEndColumn):
    if col == 3 or col == 4:    #skip longitude an latidude columns
        continue
    title+=str(data[0][col])
    if (col < dataEndColumn-1):
        title+=","
print(str(title))

s = ""
for rowNumber in printTheseRows: 
    for col in range(0,dataEndColumn):
        if col == 3 or col == 4:    #skip longitude an latidude columns
            continue
        s += str(data[rowNumber][col])
        if (col < dataEndColumn-1):             
            s+=","
    s+="\n"
print(s)
        
