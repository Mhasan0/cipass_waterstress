#input data: Fid, Column, State, Latitude, Longitude, years
myData = '13_0_Original.txt' #original dataset but with only the 13 most drough years

import numpy as np # will use this to make an array of floats. this array is the value of each state key. The aray hold the index averages for all years
import csv
f = open(myData) #1) make csv file in a tab delimited .txt
data = []
rowCount = 0
for row in f:
    data_row = row.rstrip().split('\t')  
    data.append(data_row)
    rowCount+=1

# MERGE fid-state-county columns
fidMerged = 0
countyMerged = 1
stateMerged = 2
for row in range(0,rowCount): 
    data[row][0] = data[row][fidMerged]+"-"+data[row][stateMerged]+"-"+data[row][countyMerged]

# Setting Up SOme Variables:
stateColumn = 0 #the index of the column the state is is
dataStartColumn = 3 #index where the yearly datas start from 
dataEndColumn = len(data[0]) #end of data column 
# columnsZero = dataEndColumn-dataStartColumn+1 #dataEndColumn - dataStartColumn is the pure year data column number + 1 because will add county number to index 0 
dataColumnsLength = dataEndColumn - 5 #-5 because fid,state,column,long,latid
# index = dict() # this is an dictionary that has key-value pairs to automatically group all average years for the state    

# Function to print 2d array
def printData(s):
    d = ""
    for row in range(0, rowCount):
        for col in range(0,dataEndColumn): 
            d+= str(s[row][col])
            if (col < dataEndColumn-1):             
                d+=","
        d+="\n"
    print(d)

extremes = [
    # "1801-Virginia-Franklin City",
    # "1309-Virginia-Manassas City",
    # "1284-Virginia-Falls Chruch",
    # "1665-Virginia-Bedford City",
    # "1539-Virginia-Clifton Forge",
    # "1675-Virginia-Hopewell",
    # "710-Massachusetts-Nantucket",
    # "690-Massachusetts-Dukes"
]
################################ REDUCE COUNTIES WITH ALL ZEROES ###############################################################
printTheseRows = []
printTheseRows.append(0)
# Figuring out which rows to print
for row in range(1,rowCount): #forget title row
    count = 0
    for col in range(dataStartColumn, dataEndColumn):
        if float(data[row][col]) < 1.0:
            count+=1
        data[row][col] = round(float(data[row][col]), 7)
    if count < dataColumnsLength: #if there are 13 data columns and if i get less than 13, say 12 zero counts. then at least one county isnt a 0 and so i can delete it. 
        printTheseRows.append(row)

# PUT IT INTO DATA 
s = []
rowCount = 0
for row in printTheseRows: 
    rowi = []
    
    for col in range(0,dataEndColumn):
        if col==1 or col ==2:
            continue
        if row == 0:
            rowi.append(str(data[0][col]))
            continue
        rowi.append(str(data[row][col]))
    s.append(rowi)
    rowCount+=1
data = s
dataEndColumn = len(s[0]) 
print(data[0][dataEndColumn-1])  
###################################### NORMALIZE DATA #########################################################
# For normilizing: making a max/min array to keep track of all max/min vla of each year
maxy = []
miny = []
for col in range(0, dataStartColumn): 
    print(col)
    maxy.append(0.0) # These are unimportant first spaces. lines up this index and dataStartCol in the for loop below
    miny.append(0.0)

# Find max and min of each column
for col in range(dataStartColumn, dataEndColumn): 
    maxy.append(0.0) #initial val to check for this column
    miny.append(10000000.0)
    for row in range(1,rowCount): 
        # if data[row][0] in extremes: #**DONT NORMILIZE WITHOUT MAX VALUES
        #     continue
        if float(data[row][col]) >= maxy[col]:
            maxy[col] = float(data[row][col])
        if float(data[row][col]) < miny[col]:
            miny[col] = float(data[row][col])
# use the max and min values to normilize data
for col in range(dataStartColumn, dataEndColumn): 
    for row in range(1,rowCount):
        data[row][col] = round((float(data[row][col]) - miny[col]) / (maxy[col] - miny[col]), 8)

printData(data)




# # giving each state a value for printing so that i cna make a colors column 
# stateColors = dict()
# stateCount = 0
# for row in range(1,rowCount):
#     split = data[row][0].split('-')
#     if (split[1] not in stateColors):
#         stateCount+=1
#         stateColors[split[1]] = stateCount







    
