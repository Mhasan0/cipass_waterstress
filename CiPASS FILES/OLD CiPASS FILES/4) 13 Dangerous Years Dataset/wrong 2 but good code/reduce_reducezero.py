#GOAL: take the 13 dangerous years data and get rid of rows that has 0 stress in all 13 years (so that only the endangered counties exist)
# Taking the original data (1.txt) and getting rid of all counties that is only 0
# basically: using 1.txt to make 2.txt

import numpy as np # will use this to make an array of floats. this array is the value of each state key. The aray hold the index averages for all years
import csv
f = open('reduced2.txt') #1) make csv file in a tab delimited .txt
# 0) Put in data matrix:
data = []
rowCount = 0
for row in f:
    data_row = row.rstrip().split('\t')  
    data.append(data_row)
    rowCount+=1

stateColumn = 0 #the index of the column the state is is
dataStartColumn = 1 #index where the yearly datas start from 
dataEndColumn = len(data[0]) #end of data column 
columnsZero = dataEndColumn-dataStartColumn+1 #dataEndColumn - dataStartColumn is the pure year data column number + 1 because will add county number to index 0 
printTheseRows = []

for row in range(1,rowCount): 
    for year in range(dataStartColumn,dataEndColumn):
        if float(data[row][year]) < 1.0:
            data[row][year] = 0
#         # if (float(data[row][year]) <1):
#         #     data[row][year] = 0
# # PRINTING TITLE

title = ""
for col in range(0,dataEndColumn):
    title+=data[0][col]
    if (col < dataEndColumn-1):
        title+=","
print(title+"\n")

s = ""
for row in range(1,rowCount): 
    for col in range(0,dataEndColumn):
        s += str(data[row][col])
        if (col < dataEndColumn-1):             
            s+=","
    s+="\n"
print(s)