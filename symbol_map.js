fidIndex = 0
stateIndex = 1
countyIndex = 2


if(!d3.chart) d3.chart = {};
var dispatch
d3.chart.symbol_map = function() {
  var g;
  var data;
  var width = 500;
  var height = 500;
  // var cx = 10;
  var size_scale = 60;
  dispatch = d3.dispatch(chart, "hover");
    
  var projection = d3.geo.albersUsa()
    .translate([700, 330])
    .scale([1300]);                      //SCALE OF THE DOTS ON THE MAP 

    
  function chart(container) {
    g = container;
    update();
  }
  chart.update = update;

  function update() 
  { 
    var symbols = g.selectAll("circle")
      .data(data.sort(function(a, b) { 
        return b.agprod - a.agprod; 
      }), key);
      symbols.enter().append("circle");
      
      
      symbols.transition()
        .attr("cx", function(d) {
          // console.log(d)
          // console.log([d.lon, d.lat])
          // console.log(projection([d.lon, d.lat])[0])
          return projection([d.lon, d.lat])[0];
        })
        .attr("cy", function(d) {
          return projection([d.lon, d.lat])[1];
        })
        .attr("r", function(d) {
          return Math.sqrt(parseFloat(d.agprod) * size_scale);
        })
      

        .style("fill", function(d) {
          return d.color;
        })
        .style("opacity", .8)
        .attr("class", "symbol")
        .attr("id", function(d) { return d.id; }); ///***THIS IS THE DOT THAT THE HOVER SELECTS!!! THIS IS THE "this" in the hover events */
        // .attr("title", function(d) { return "NDI for " + d.id + ": " + d.agprod; }); ///***THIS IS THE DOT THAT THE HOVER SELECTS!!! THIS IS THE "this" in the hover events */

    
    symbols.exit()
      .remove();


    symbols.on("mouseover", function(d) 
    {
      console.log("ON HOVER: "+d.id+"   lat: "+d.lat+"  lon: "+d.lon+" ") // of d["id"]
  
      // d3.select(this).style("fill", "blue")                ///***What color is the ring of the dot i hover on? stoke or fill dot */
      // d3.select(this).style("r", 20)                       //***When i hover on dot, make it bigger by changing the dot's html "r" property */                                               
      // dispatch.hover([d])
      selectDotById(d.id, "blue");
      printId(d.id)
      stateAll
                                           
    })

    symbols.on("mouseout", function(d) {
      console.log("OUT OF HOVER: "+d["id"]+"   lat: "+d["lat"]+"  lon: "+d["lon"]+" ")
      deselectDotById(d.id)
      printId()
    })
  }

  /////////////////////////////////////////////////////////////////////////

    // chart.highlight = function(data) {
    //     var symbols = g.selectAll("circle.symbol")
    //     .style("stroke", "")
    //     .style("stroke-width", "")
        
    //     symbols.data(data, key)
    //     .style("stroke", "black")
    //     .style("stroke-width", 3)
    // }

  chart.data = function(value) {
    if(!arguments.length) return data;
    data = value;
    return chart;
  }
  chart.width = function(value) {
    if(!arguments.length) return width;
    width = value;
    return chart;
  }
  chart.height = function(value) {
    if(!arguments.length) return height;
    height = value;
    return chart;
  }
    
    var key = function(d) {
        return d.id;
    };

  return d3.rebind(chart, dispatch, "on");
}

//////////////////////////////////////////////////////////////////////

//***MY FUNCTION TO SELECT A DOT AND HILIGHT IT */
function selectDotById(id, color = "red"){
  dotItem = document.getElementById(id);   
  d3.select(dotItem).style("fill", color) 
  d3.select(dotItem).style("r", 20)                      
  dispatch.hover([])
}

//***MY FUNCTION TO DE-SELECT A DOT*/
function deselectDotById(id, color = "red"){
  dotItem = document.getElementById(id);   
  d3.select(dotItem).style("fill", color) 
  d3.select(dotItem).style("r", 7.745966692414834)                      
  dispatch.hover([])
}

//***MY FUCNTION TO PRINT COUNTIES SELECTED IN HTML BULLE TPOINTS */
function printCountiesDiv(printSet){
  var mainList = document.getElementById("printUl");
  while((lis = mainList.getElementsByTagName("li")).length > 0) {
    mainList.removeChild(lis[0]);
  }

  for(var i=0;i<printSet.size;i++){
      var item = printSet.getByIndex(i);
      var split = item.split('-')
      var item = split[countyIndex]+", "+split[stateIndex]+"       FID: "+split[fidIndex] 
      var elem = document.createElement("li");
      elem.innerHTML=item;
      mainList.appendChild(elem);

  }    
}
function printId(item){
  var mainList = document.getElementById("printUl");
  while((lis = mainList.getElementsByTagName("li")).length > 0) {
    mainList.removeChild(lis[0]);
  }
  var split = item.split('-')
  var item = split[countyIndex]+", "+split[stateIndex]+"       FID: "+split[fidIndex] 
  var elem = document.createElement("li");
  elem.innerHTML=item;
  mainList.appendChild(elem);
}
function printIdStr(str,item){
  var mainList = document.getElementById("printUl");
  while((lis = mainList.getElementsByTagName("li")).length > 0) {
    mainList.removeChild(lis[0]);
  }
  var split = item.split('-')
  var item = split[countyIndex]+", "+split[stateIndex]+"       FID: "+split[fidIndex] 
  var elem = document.createElement("li");
  elem.innerHTML=str+item;
  mainList.appendChild(elem);

}


