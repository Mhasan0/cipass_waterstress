// var mapdot_json = "/Pattern1/13_Dots_Pattern1.json" //This pots dots on state map, i can then print the dots im hovering on later in the code

var Vis = {}; /**A dynamic, browser based visualization library */
var width = 1600,
height = 700;
var svg = d3.select("#state").append("svg")
    .attr("width", width)
	.attr("height", height);
var mapgroup = svg.append("g");
var projection = d3.geo.albersUsa() //the state picture map sacle is how big is
    .translate([700, 330])
    .scale([1300]);
var path = d3.geo.path()
	.projection(projection);

// S1
queue()
	.defer(d3.json, "/files/us.json")
	.await(ready);
// S2
function ready(error, us) 
{
	//make the state map picture
	mapgroup.append("path")
		.attr("class", "states")
		.datum(topojson.feature(us, us.objects.states))
		.attr("d", path);
	loadJson(mapdot_json); //**load the dots
	//loadGSS(SPREADSHEET_ID, SPREADSHEET_TAB);
}
// S3
da = []
function loadJson(file_name) 
{
	d3.json(file_name, function(error, file_data) {
		Vis.data = file_data;
		// Vis.data.forEach(function(d) {
		// 	// d.created *= 1000;							////*** */accessing the created element in the json file and chnageing it
		// });
		createDiagram();
	});        
}
// S4tooltip
var symbol_map = d3.chart.symbol_map()
function createDiagram() 
{
	symbol_map.data(Vis.data)              // data state pic only. goes into symbol_state.js to mak emap
	symbol_map(mapgroup)
	state_Hover()
}
function state_Hover(){
	//***** WHen i hover on a dota, it will call this function, this will call the sybmol_map.on function to update dot, will then recall this fuction when i unhover on dot to update the dot
	// console.log(Vis.data[1])											//****This is the data that will be passed onto th e"hover" parameter! */
	symbol_map.on("hover", function(hovered) { 							//**Print what state im hovering on */
		if (hovered.length != 0){
			console.log("*    "+hovered[0]["id"]+"	lon: "+hovered[0]["lon"]+"	lat: "+hovered[0]["lat"])
			// console.log("*"+symbol_map.symbol)
		} 
		    
	})
}