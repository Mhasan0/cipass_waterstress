var stateColumn = "FID-State-County"

function colorGraph(column)  //*i passed the 2nd columns and so am colorGraph base don it: 1952
{ 
	parallelG.svg.selectAll(".column")
		.style("font-weight", "normal")
		.filter(function(d) { return d == column; })
		.style("font-weight", "bold");
	// Find the domain of the color values, will pass it next		
	var values = parallelG.data().map(function(d){
		return parseFloat(d[column])
	}); 
	// FINALLY PASSING DOMAIN VALUES I FOUND AND SETTING THEM
	color_set.domain([d3.min(values), d3.max(values)]); //k0*passes values from the column i picked, this one is the 2nd column: 1952 so range [0, 0.16]
	parallelG.color(function(d){
		return color_set([d[column]])
	}).render();
};	
// 3) activates every times i move mouse
function highlightLineOnClick(mouseClick, drawTooltip){
	var clicked = [];
    var clickedCenPts = [];
	clickedData = getClickedLines(mouseClick);
	if (clickedData && clickedData[0].length!=0)
	{
		clicked = clickedData[0];
		clickedCenPts = clickedData[1];
		for (var i=0; i<clickedData[0].length; ++i)
		{
			clickedStateId = clicked[i][stateColumn] //***Will use this variable to deselect the dot ons tate map */
			console.log("brushingdPS.1 "+isBrushed);
			selectDotById(clicked[i][stateColumn])		//***MY FUNCTION TO SELEC THE DOTS IM HOVERING MY LINE ON */
			if (isBrushed == 0){
				printId(clicked[i][stateColumn])
				// Now that i have selected the dots i have brushed, i will now gray out the remaining dots
				for (var i=0; i<allStates.size; ++i){
					if (allStates.getByIndex(i) != clickedStateId){
						console.log("Selecting: "+clickedStateId);
						selectedHovered.add(clickedStateId)
						deselectDotById(allStates.getByIndex(i), "#DCDCDC") //DULL HIGHLIGHT
					}
					// console.log("Hover Selected: "+selectedHovered);
				}
			}
			printIdStr("Peeking: ",clicked[i][stateColumn])


		}
		// highlight clicked linE
		parallelG.highlight(clicked);
		if (drawTooltip){
			cleanTooltip();			// clean if anything is there
			addTooltip(clicked, clickedCenPts);	    	// add tooltip
		}	
	}
};
// 4) activates every time i move mouse
var isBrushed = false
function getClickedLines(mouseClick){
    var clicked = [];
    var clickedCenPts = [];
	// find which data is activated right now
	var activeData = getActiveData();					//**get info on what i highlighted */

	// find centriod points
	var graphCentPts = getCentroids(activeData);
    if (graphCentPts.length==0) return false;
	// find between which axes the point is
    var axeNum = findAxes(mouseClick, graphCentPts[0]);
    if (!axeNum) return false;
    graphCentPts.forEach(function(d, i){
	    if (isOnLine(d[axeNum-1], d[axeNum], mouseClick, 2)){
	    	clicked.push(activeData[i]);
	    	clickedCenPts.push(graphCentPts[i]); 					// for tooltip
	    }
	});
	if (isBrushed == false){
		for (var i=0; i<allStates.size; ++i){
			deselectDotById(allStates.getByIndex(i))	//COMPLETE DESELECTION
		}
	}
	
	return [clicked, clickedCenPts]
}

////getCLickedLinea() Support://///
function getActiveData(){
	// I'm pretty sure this data is already somewhere in graph
	if (parallelG.brushed()!=false) 
		return parallelG.brushed(); //*** false=shows point when hovering 
	return parallelG.data();
}
// Add highlight for every line on click. filters the lines so only shows the one u hovering on
function getCentroids(data)
{
	// this function returns centroid points for data. I had to change the source for parallelcoordinates and make compute_centroids public. I assume this should be already somewhere in graph and I don't need to recalculate it but I couldn't find it so I just wrote this for now
	var margins = parallelG.margin();
	var graphCentPts = [];
	// console.log('saaa       '+Object.values(data));

	data.forEach(function(d){
		var initCenPts = parallelG.compute_centroids(d).filter(function(d, i){
			return i%2==0;
		});
		// console.log(Object.values(d)[0].length)
		// console.log('saaa       '+Object.values(d)[0]);
		// selectDotById(Object.values(d)[0])

		// move points based on margins
		var cenPts = initCenPts.map(function(d){
			return [d[0] + margins["left"], d[1]+ margins["top"]]; 
		});

		graphCentPts.push(cenPts);
	});
	return graphCentPts;
}
function findAxes(testPt, cenPts){
	// finds between which two axis the mouse is
	var x = testPt[0];
	var y = testPt[1];
	// make sure it is inside the range of x
	if (cenPts[0][0] > x) return false;
	if (cenPts[cenPts.length-1][0] < x) return false;

	// find between which segment the point is
	for (var i=0; i<cenPts.length; i++){
		if (cenPts[i][0] > x) return i;
	}
}
//i think this calculates if im on the line
function isOnLine(startPt, endPt, testPt, tol){
	// check if test point is close enough to a line
	// between startPt and endPt. close enough means smaller than tolerance
	var x0 = testPt[0];
	var	y0 = testPt[1];
	var x1 = startPt[0];
	var	y1 = startPt[1];
	var x2 = endPt[0];
	var	y2 = endPt[1];
	var Dx = x2 - x1;
	var Dy = y2 - y1;
	var delta = Math.abs(Dy*x0 - Dx*y0 - x1*y2+x2*y1)/Math.sqrt(Math.pow(Dx, 2) + Math.pow(Dy, 2)); 
	if (delta <= tol) return true;
	return false;
}

function cleanTooltip(){
	// removes any object under #tooltip is
	parallelG.svg.selectAll("#tooltip")
    	.remove();
}
function addTooltip(clicked, clickedCenPts){
	// sdd tooltip to multiple clicked lines
    var clickedDataSet = [];
	var margins = parallelG.margin()
    // get all the values into a single list
    // I'm pretty sure there is a better way to write this is Javascript
    for (var i=0; i<clicked.length; i++){
    	for (var j=0; j<clickedCenPts[i].length; j++){
    		var text = d3.values(clicked[i])[j];
  			// not clean at all!
  			var x = clickedCenPts[i][j][0] - margins.left;
  			var y = clickedCenPts[i][j][1] - margins.top;
  			clickedDataSet.push([x, y, text]);
		}
	};

	// add rectangles
	var fontSize = 14;
	var padding = 2;
	var rectHeight = fontSize + 2 * padding; //based on font size

	// console.log("sssss     "+clickedDataSet);
	parallelG.svg.selectAll("rect[id='tooltip']")
		.data(clickedDataSet).enter()
		.append("rect")
		.attr("x", function(d) { 
			return d[0] - d[2].length * 5;})	//shift blackground box leftward
		.attr("y", function(d) { 
			return d[1] - rectHeight + 1 * padding; }) //location of the text box, up or down
		.attr("rx", "2")
		.attr("ry", "2")
		.attr("id", "tooltip")
		.attr("fill", "grey")                 					//hovering line number box color                
		.attr("opacity", .9)
		.attr("width", function(d){
			return d[2].length * 10+10;})	//extendign the box right side //hovering line number box width
		.attr("height", rectHeight);
	// add text on top of rectangle
	parallelG.svg.selectAll("text[id='tooltip']")
    	.data(clickedDataSet).enter()
    		.append("text")
			.attr("x", function(d) { return d[0];})
			.attr("y", function(d) { return d[1]; })
			.attr("id", "tooltip")
			.attr("fill", "white")									//hovering line number box text colro
			.attr("text-anchor", "middle")
			.attr("font-size", fontSize)
        	.text( function (d){ return d[2];})    
}




